package com.corecanarias.agora.client.subsystem;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.naming.Context;

import org.hornetq.jms.client.HornetQConnectionFactory;

import com.corecanarias.agora.jboss.services.ServiceLocator;

public class MessageService implements MessageListener {
	private static final String QUEUE_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
	private ConnectionFactory connectionFactory;
	private Connection connection;
	private Session session;
	private String destinationJndi;
	private MessageConsumer consumer;
	private HashMap<String, MessageListener> messageListener;

	public MessageService(String destination) {

		this.destinationJndi = destination;
		messageListener = new HashMap<String, MessageListener>();
	}

	public void connect() {
		boolean connected = false;
		while(!connected) {
			try {
				connected = true;
				internalConnect();
			} catch (Exception e) {
				connected = false;
				e.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {}
			}					
		}
		System.out.println("Done");
	}

	protected void internalConnect() throws JMSException {
		connectionFactory = getJmsConnectionFactory(QUEUE_CONNECTION_FACTORY);
		
		HornetQConnectionFactory r = (HornetQConnectionFactory)connectionFactory; 
		r.setThreadPoolMaxSize(1);
		String username = ServiceLocator.getInstance().getSettings().getConfig().getString(Context.SECURITY_PRINCIPAL);
		if(username != null) {
			String password = ServiceLocator.getInstance().getSettings().getConfig().getString(Context.SECURITY_CREDENTIALS);
			connection = connectionFactory.createConnection(username, password);			
		} else {
			connection = connectionFactory.createConnection();
		}
		connection.setExceptionListener(new ExceptionListener() {

			@Override
			public void onException(JMSException exception) {
				disconnect();
				connect();
			}				
		});
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = getJmsDestination(destinationJndi);

		consumer = session.createConsumer(destination);
		connection.start();
		consumer.setMessageListener(this);
	}
	
	public Session getSession() {
		return session;
	}
	
	public void disconnect() {

		try {
			if(session != null) {
				session.close();				
			}
			if(connection != null) {
				connection.close();				
			}
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	private ConnectionFactory getJmsConnectionFactory(String jmsConnectionFactoryJndiName) {
		return (ConnectionFactory) ServiceLocator.getInstance().lookup(jmsConnectionFactoryJndiName);
	}

	private Destination getJmsDestination(String jmsDestinationJndiName) {
		return (Destination) ServiceLocator.getInstance().lookup(jmsDestinationJndiName);
	}

	public void addMessageListener(String type, MessageListener listener) {
	
		messageListener.put(type, listener);
	}

	public void onMessage(Message arg0) {
		Iterator<Entry<String, MessageListener>> iter = messageListener.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, MessageListener> entry = iter.next();
			entry.getValue().onMessage(arg0);
		}
	}
}

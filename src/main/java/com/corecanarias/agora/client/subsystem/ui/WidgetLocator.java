package com.corecanarias.agora.client.subsystem.ui;


public interface WidgetLocator {
	
	Object getWidgetById(String id);
	
	/**
	 * Devuelve el panel sobre el que se encuentran los componentes
	 * @return
	 */
	Object getWindow();
}

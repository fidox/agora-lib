package com.corecanarias.agora.client;

public interface ProgressBarView {

	void setMax(int max);
	void step();
	void clear();
	void activateProgressBar();
	void stopProgressBar();
}

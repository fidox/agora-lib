package com.corecanarias.agora.lib;

public class MathUtils {

	public static double roundHundreds(double d) {
		return Math.round(d * 100D) / 100D;		
	}
	
	public static final int max(int a, int b) {
		return a > b ? a : b;
	}
	
	public static final long max(long a, long b) {
		return a > b ? a : b;
	}
	
	public static final long min(long a, long b) {
		return a <= b ? a : b;
	}
}

/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

	
/**
 * Accesor to configuration services
 * 
 * @author Israel E. Bethencourt (ieb@corecanarias.com)
 *
 */
public interface Configuration {

	String[] getModules();
	
	ModuleConfiguration getModule(String name);
	
	/**
	 * Get a single property
	 * @param module the module
	 * @param name the property name
	 * @return The value of the module/name or null if doesn't exists
	 */
	Object getProperty(String module, String name);
	
	/**
	 * Get a single property as string specifying a default value
	 * @param module the module
	 * @param name the property name
	 * @param defaultValue If the property is not defined. this value is returned instead.
	 * @return The value of the module/name or null if doesn't exists
	 * @return
	 */
	String getPropertyAsString(String module, String name, String defaultValue);

	String getPropertyAsString(String module, String name);

	/**
	 * Get a multi-value property. You can define multi-value properties as follow:
	 * 
	 " <property name="prop-list"> 
	 "    <value>val1</value> 
	 "    <value>val2</value> 
	 "    <value>val3</value>
     "  </property>
	 *  
	 * 
	 * @param module The module
	 * @param name Property name defined as list
	 * @return The value list
	 */
	String[] getPropertyAsList(String module, String name);
	
	/**
	 * Get a single property as boolean. If the parameter is not defined returns false
	 * 
	 * @param module The module
	 * @param name The property name
	 * @return true if the property is 'true' false otherwise
	 */
	boolean getPropertyAsBoolean(String module, String name);
	
	/**
	 * Get a single property as boolean. If the parameter is not defined returns the default value
	 * 
	 * @param module The module
	 * @param defaultValue the default value
	 * @param name The property name
	 * @return the property value of module/name or defaultValue is the property is not defined
	 */
	boolean getPropertyAsBoolean(String module, String name, boolean defaultValue);
	
	/**
	 * Get a single property as integer
	 * @param module The module
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	int getPropertyAsInt(String module, String name, int defautlValue);
	
	/**
	 * Get a single property as float
	 * @param module The module
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	float getPropertyAsFloat(String module, String name, float defautlValue);
	
	/**
	 * Get a single property as double
	 * @param module The module
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	double getPropertyAsDouble(String module, String name, double defautlValue);
	
	String printConfigurationData();
}
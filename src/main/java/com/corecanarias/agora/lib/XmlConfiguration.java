/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlConfiguration extends BaseConfiguration {
	private LinkedHashMap<String, ModuleConfiguration> props;

	public XmlConfiguration() {
		
	}
	
	public XmlConfiguration(InputStream xml) {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xml);
			Element root = doc.getDocumentElement();
			
			updateProperties(root);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Map<String, ModuleConfiguration> updateProperties(Element root) {
		props = new LinkedHashMap<String, ModuleConfiguration>();
		if (root == null) {
			return props;
		}
		NodeList modules = root.getChildNodes();
		for(int i = 0; i < modules.getLength(); i++) {
			if("module".equals(modules.item(i).getNodeName())) {
				Node module = modules.item(i);
				parseModule(module);
			}
		}
		return props;
	}

	private void parseModule(Node module) {
		BaseModuleConfiguration moduleConfig = new BaseModuleConfiguration(module.getAttributes().getNamedItem("name").getTextContent());
		
		props.put(module.getAttributes().getNamedItem("name").getTextContent(), moduleConfig);
		NodeList properties = module.getChildNodes();
		parseProperties(properties, moduleConfig);
	}

	/**
	 * Indica si un elemento <property> tiene su valor definido dentro del nodo. Es decir, es del tipo:
	 * <property name="nombre">value</property>
	 * En este caso se coge el contenido del nodo como valor del elemento
	 * Para determinar esto se debe cumplir que no haya ningún elemento dentro del nodo
	 * @param property
	 * @return
	 */
	private boolean isValuedProperty(Node property) {
		
		for(int i = 0; i < property.getChildNodes().getLength(); i++) {
			Node item = property.getChildNodes().item(i);
			if(item.getNodeType() == Node.ELEMENT_NODE) {
				return false;
			}
		}
		return true;
	}
	
	private void parseProperties(NodeList properties, BaseModuleConfiguration moduleConfig) {
		for(int j = 0; j < properties.getLength(); j++) {
			if("property".equals(properties.item(j).getNodeName())) {
				Node property = properties.item(j);
				if(property.getAttributes().getNamedItem("value") != null) {
					moduleConfig.put(property.getAttributes().getNamedItem("name").getTextContent(), property.getAttributes().getNamedItem("value").getTextContent());
				} else if(isValuedProperty(property)) {
					moduleConfig.put(property.getAttributes().getNamedItem("name").getTextContent(), property.getTextContent());
				} else {

					List<String> listValues = parseListProperties(property.getChildNodes());
					moduleConfig.put(property.getAttributes().getNamedItem("name").getTextContent(), listValues);					
				}
			}
		}
	}

	private List<String> parseListProperties(NodeList listItems) {
		List<String> res = new ArrayList<String>();
		for(int j = 0; j < listItems.getLength(); j++) {
			if("value".equals(listItems.item(j).getNodeName())) {
				res.add(listItems.item(j).getTextContent());				
			}
		}
		return res;
	}

	@Override
	public Map<String, ModuleConfiguration> getConfiguration() {
		return props;
	}
}
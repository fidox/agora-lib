package com.corecanarias.agora.lib;

import java.io.File;

public interface ZipUtilEntryName {

	String getEntryName(File f);
}
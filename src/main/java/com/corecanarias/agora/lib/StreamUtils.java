/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.apache.commons.lang.StringUtils;

public class StreamUtils {

	private StreamUtils() {
		
	}
	
	/**
	 * Copy a InputStream into another and notify the process of the copy. You are encourged to open and close the streams.
	 * Also you can cancel the copy by setting the propagationId of any PROGRESS PropertyChangeEvent to CANCEL
	 * 
	 * @param in Source stream
	 * @param out Dest stream
	 * @param progress Listener to get the progress notifications
	 * @return True if it was sucessfully copied false if it was cancelled by the user
	 * @throws IOException In case of read/write problems
	 */
	public static final boolean copyInputStream(InputStream in, OutputStream out, PropertyChangeListener progress) throws IOException {
		byte[] buffer = new byte[1024];
		int len;
		long count = 0;
		while ((len = in.read(buffer)) >= 0) {
			count += len;
			if(progress != null) {					
				PropertyChangeEvent evt = new PropertyChangeEvent(progress, "PROGRESS", len, count);
				progress.propertyChange(evt);
				if(StringUtils.equals((String)evt.getPropagationId(), "CANCEL")) {
					return false;
				}
			}
			out.write(buffer, 0, len);
		}
		return true;
	}

	/**
	 * Save InputStream to file
	 * @param is The stream to save
	 * @param destFile The destination file
	 * @return The destination file
	 */
	public static File saveToFile(InputStream is, File destFile) {
		FileOutputStream dest;
		try {
			dest = new FileOutputStream(destFile);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}

		try {
			copyInputStream(is, dest, null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally  {
			try {
				dest.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		return destFile;
	}
	
	public static String inputStreamToString(InputStream is) {
		try {
			StringBuffer res = new StringBuffer();
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while((line = in.readLine()) != null) {
			  res.append(line);
			}		
			return res.toString();			
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
}
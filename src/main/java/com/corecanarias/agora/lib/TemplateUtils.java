/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.text.StrSubstitutor;

public class TemplateUtils {

	private TemplateUtils() {
		
	}
	
	public static final Map<String, String> getProperties(String prefix, Object obj) {
		Map<String, String> res = new HashMap<String, String>();
		try {
			Map<String, Object> map = org.apache.commons.beanutils.BeanUtils.describe(obj);
			for(Entry<String, Object> e: map.entrySet()) {
				res.put(prefix + "." + e.getKey(), (String)e.getValue());
			}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}

		return res;
	}
	
	public static final String replaceVars(String s, Map<String, String> params) {
		 StrSubstitutor sub = new StrSubstitutor(params);
		 return sub.replace(s);
	}
}
package com.corecanarias.agora.lib;

import java.util.HashMap;
import java.util.Map;

public class HashMapEasy<K,V> extends HashMap<K,V> {
	private static final long serialVersionUID = -7455106707612446240L;
	
	public HashMapEasy(Map<K, V> map) {
		super(map);
	}

	public HashMapEasy() {
	}

	/**
	 *  
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public V get(K key, V defaultValue) {
		V k = get(key);
		if(k == null) {
			return defaultValue;
		}
		return k;
	}
	
	public Integer getAsInteger(K key) {
		Object k = get(key);
		if(k == null) {
			return null;
		}
		
		return Integer.parseInt(String.valueOf(k));
	}
	
	public Integer getAsInteger(K key, Integer defaultValue) {
		Integer res;
		try {
			res = getAsInteger(key);			
		} catch(NumberFormatException e) {
			return defaultValue;
		}
		if(res == null) {
			return defaultValue;
		}
		return res;
	}
}
/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class BaseConfiguration implements Configuration {
	
	public abstract Map<String, ModuleConfiguration> getConfiguration();

	public String[] getModules() {

		List<String> modules = new ArrayList<String>();
		for(Entry<String, ModuleConfiguration> module: getConfiguration().entrySet()) {
			modules.add(module.getKey());
		}
		return modules.toArray(new String[] {}); 
	}
	
	public ModuleConfiguration getModule(String name) {
		return getConfiguration().get(name);
	}
	
	/**
	 * Get a single property as string
	 * @param module the module
	 * @param name the property name
	 * @return The value of the module/name or null if doesn't exists
	 */
	public Object getProperty(String module, String name) {
		ModuleConfiguration m = getModule(module);
		if(m == null) {
			return null;
		}
		return m.getProperty(name);
	}
	
	/**
	 * Get a single property as string specifying a default value
	 * @param module the module
	 * @param name the property name
	 * @param defaultValue If the property is not defined. this value is returned instead.
	 * @return The value of the module/name or null if doesn't exists
	 * @return
	 */
	public String getPropertyAsString(String module, String name, String defaultValue) {
		Object value = getProperty(module, name);

		if(value == null) {
			return defaultValue;
		}
		return value.toString();
	}

	public String getPropertyAsString(String module, String name) {
		Object value = getProperty(module, name);

		if(value == null) {
			return "";
		}
		return value.toString();
	}

	/**
	 * Get a multi-value property. You can define multi-value properties as follow:
	 * 
	 " <property name="prop-list"> 
	 "    <value>val1</value> 
	 "    <value>val2</value> 
	 "    <value>val3</value>
     "  </property>
	 *  
	 * 
	 * @param module The module
	 * @param name Property name defined as list
	 * @return The value list
	 */
	@SuppressWarnings("unchecked")
	public String[] getPropertyAsList(String module, String name) {
		ModuleConfiguration m = getModule(module);
		if(m == null) {
			return null;
		}
		return m.getPropertyAsList(name);
	}
	
	/**
	 * Get a single property as boolean. If the parameter is not defined returns false
	 * 
	 * @param module The module
	 * @param name The property name
	 * @return true if the property is 'true' false otherwise
	 */
	public boolean getPropertyAsBoolean(String module, String name) {
		return getPropertyAsBoolean(module, name, false);
	}

	/**
	 * Get a single property as boolean. If the parameter is not defined returns the default value
	 * 
	 * @param module The module
	 * @param defaultValue the default value
	 * @param name The property name
	 * @return the property value of module/name or defaultValue is the property is not defined
	 */
	public boolean getPropertyAsBoolean(String module, String name, boolean defaultValue) {
		Object res = getProperty(module, name);
		if(res == null) {
			return defaultValue;
		}
		return "true".equals(res);
	}

	/**
	 * Get a single property as integer
	 * @param module The module
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	public int getPropertyAsInt(String module, String name, int defaultValue) {
		Object res = getProperty(module, name);

		if(res == null) {
			return defaultValue;
		}
		try {
			return Integer.valueOf(res.toString());			
		} catch(NumberFormatException e) {
			throw new ConfigurationException("Unable to convert " + module + "/" + name + " to integer: " +  res, e);
		}
	}
	
	/**
	 * Get a single property as float
	 * @param module The module
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	public float getPropertyAsFloat(String module, String name, float defaultValue) {
		Object res = getProperty(module, name);
		if(res == null) {
			return defaultValue;
		}

		try {
			return Float.valueOf(res.toString());			
		} catch(NumberFormatException e) {
			throw new ConfigurationException("Unable to convert " + module + "/" + name + " to float: " +  res, e);
		}
	}
	
	/**
	 * Get a single property as double
	 * @param module The module
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	public double getPropertyAsDouble(String module, String name, double defaultValue) {
		Object res = getProperty(module, name);
		if(res == null) {
			return defaultValue;
		}

		try {
			return Double.valueOf(res.toString());			
		} catch(NumberFormatException e) {
			throw new ConfigurationException("Unable to convert " + module + "/" + name + " to double: " +  res, e);
		}
	}
	
	public String printConfigurationData() {
		if(getConfiguration() == null) {
			return null;
		}
		
		StringBuffer res = new StringBuffer();
		res.append("CONFIGURATION:\n");
		for(Entry<String, ModuleConfiguration> item: getConfiguration().entrySet()) {
			res.append(item.getValue().printConfigurationData());
		}
		return res.toString();
	}
}

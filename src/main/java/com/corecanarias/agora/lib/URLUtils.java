package com.corecanarias.agora.lib;

public class URLUtils {

	public static final String getURLBase(String url) {
		if(url == null) {
			return null;
		}
		String[] parts = url.split("/");
		return parts[parts.length -1];
	}
}
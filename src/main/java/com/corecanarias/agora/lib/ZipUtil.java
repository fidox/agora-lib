package com.corecanarias.agora.lib;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipUtil {

	public static void Unzip(InputStream stream, PropertyChangeListener progress, ZipUtilOutputFilter zipUtilOutputFilter) throws IOException {
		ZipInputStream zipStream = new ZipInputStream(stream);

		ZipEntry entry;	
		while((entry = zipStream.getNextEntry()) != null) {
			File destFileEntry = zipUtilOutputFilter.outputFilter(entry.getName());

			if(!entry.isDirectory()) {
				if(progress != null) {					
					PropertyChangeEvent evt = new PropertyChangeEvent(progress, "START", entry.getName(), destFileEntry.length());
					progress.propertyChange(evt);
				}
				if(!destFileEntry.getParentFile().exists()) {
					if(!destFileEntry.getParentFile().mkdirs()) {
						throw new RuntimeException("Unable to create directory " + destFileEntry.getParentFile());
					}
				}
				
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(destFileEntry));
				FileUtil.copyInputStream(zipStream, out, progress);
				out.close();	
				if(progress != null) {					
					PropertyChangeEvent evt = new PropertyChangeEvent(progress, "END", 0, 0);
					progress.propertyChange(evt);
				}				
			}
		}
		zipStream.close();		
	}
	
	public static void Unzip(String file, PropertyChangeListener progress, ZipUtilOutputFilter zipUtilOutputFilter) throws IOException {
		ZipUtil.Unzip(new FileInputStream(new File(file)), progress, zipUtilOutputFilter);
	}

	public static final void Unzip(final String dest, String file, PropertyChangeListener progress) throws IOException {
		Unzip(file, progress, new ZipUtilOutputFilter() {

			@Override
			public File outputFilter(String file) {
				return new File(dest, file);
			}			
		});
	}
	
	public static final void zip(String orig, String dest, PropertyChangeListener progress) throws IOException {

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(dest));
		File origDir = new File(orig);
        zipFile(out, origDir, orig, progress);
		out.close();
	}

	private static void zipFile(ZipOutputStream out, File origDir, String orig, PropertyChangeListener progress) throws IOException {
		File[] origContent = origDir.listFiles();
		
		for(File f: origContent) {
			// Add ZIP entry to output stream.
			
		    String relativeName = f.getAbsolutePath();
		    relativeName = relativeName.substring(orig.length() +1);
		    if(f.isDirectory()) {
		    	relativeName = relativeName + "/";
		    }
		    relativeName = relativeName.replaceAll(java.util.regex.Matcher.quoteReplacement("\\"), "/");
		    ZipEntry zipEntry = new ZipEntry(relativeName);
			out.putNextEntry(zipEntry);
			if(!f.isDirectory()) {
			    FileInputStream in = new FileInputStream(f);			    
			    
				if(progress != null) {					
					PropertyChangeEvent evt = new PropertyChangeEvent(progress, "START", zipEntry.getName(), f.length());
					progress.propertyChange(evt);
				}

				FileUtil.copyInputStream(in, out, progress);            	
			    in.close();

			    if(progress != null) {					
					PropertyChangeEvent evt = new PropertyChangeEvent(progress, "END", 0, 0);
					progress.propertyChange(evt);
				}

			} else {
				
				zipFile(out, f, orig, progress);
			}
			out.closeEntry();			
		}
	}

	public static boolean zip(String[] files, String dest, ZipUtilEntryName entryNames, PropertyChangeListener progress) throws IOException {

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(dest));

		for(String s: files) {
			File file = new File(s);
		    ZipEntry zipEntry;
			if(entryNames == null) {
				zipEntry = new ZipEntry(file.getName());
			} else {
				zipEntry = new ZipEntry(entryNames.getEntryName(file));
			}
			out.putNextEntry(zipEntry);
			if(progress != null) {					
				PropertyChangeEvent evt = new PropertyChangeEvent(progress, "START", "Packaging " + zipEntry.getName(), file.length());
				progress.propertyChange(evt);	
			}

		    FileInputStream in = new FileInputStream(file);			    
			if(!FileUtil.copyInputStream(in, out, progress)) {
				return false;
			}
		    in.close();

		    if(progress != null) {					
				PropertyChangeEvent evt = new PropertyChangeEvent(progress, "END", 0, 0);
				progress.propertyChange(evt);
			}
		}
		out.closeEntry();			
		out.close();
		return true;
	}
	
	public static boolean zip(String[] files, String dest, PropertyChangeListener progress) throws IOException {

		return zip(files, dest, new ZipUtilEntryName() {

			@Override
			public String getEntryName(File f) {
				return f.getName();
			}
			
		}, progress);
	}
}

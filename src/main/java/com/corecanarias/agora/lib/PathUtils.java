/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

public class PathUtils {

	private PathUtils() {
		
	}
	public static String appendPath(String orig, String append) {
		if(orig == null) {
			orig = "";
		}
		if(append == null) {
			append = "";
		}
		if(!orig.endsWith(File.separator)) {
			orig = orig.concat(File.separator);
		}
		orig = orig.concat(append);
		return orig;
	}

	public static String standardPath(String path) {
		if(path == null) {
			return "";
		}
		path = path.replaceAll(java.util.regex.Matcher.quoteReplacement("\\"), "/");
		if(path.indexOf(":") == 1) {
			path = path.substring(2);
		} else if(path.startsWith("//")) {
			path = path.substring(2);
			path = path.substring(path.indexOf("/"));
		}
		return path;
	}
	
	public static String buildPath(String... parts) {
		if(parts == null) {
			return "";
		}
		String res = "";
		
		for(String s: parts) {
			if(s == null) {
				s = "";
			}
			if(!StringUtils.isEmpty(res) && !res.endsWith(File.separator) && !s.trim().startsWith(File.separator)) {
				res = res.concat(File.separator);								
			}
			res = res.concat(s);
		}
		return res;
	}
	
	public static String[] splitExtension(String filename) {
		if(filename == null) {
			return null;
		}
		String[] res = new String[2];
		int idx = filename.lastIndexOf(".");
		
		res[0] = filename.substring(0, idx);
		res[1] = filename.substring(idx);
		return res;
	}
	
	public static String windowsNormalize(String path) {
		if(path == null) {
			return "";
		}
		return path.replaceAll("/", java.util.regex.Matcher.quoteReplacement("\\"));
	}
	
	public static final String getCanonicalPath(File file) {
		if(file == null) {
			return "";
		}
		try {
			return file.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("Unable to canonize path: -" + file + "-", e);
		}
	}
	
	public static final List<File> getFilesRelativeTo(List<File> files, File relativeTo) {
		if(files == null) {
			return null;
		}
		ArrayList<File> res = new ArrayList<File>();
		for(File f: files) {
			res.add(new File(pathRelativeTo(f, relativeTo)));
		}
		return res;
	}
	
	public static String pathRelativeTo(File path, File relativeTo) {
		if(path == null) {
			throw new IllegalArgumentException("parameter path can not be null");
		}
		if(relativeTo == null) {
			relativeTo = new File(path.getParent());
		}
		String[] relativeToParts = getCanonicalPath(relativeTo).split(File.separator);
		String[] pathParts = getCanonicalPath(path).split(File.separator);
		int idxToCommonPart = 0;
		while(idxToCommonPart < pathParts.length && idxToCommonPart < relativeToParts.length && StringUtils.equals(pathParts[idxToCommonPart], relativeToParts[idxToCommonPart])) {
			idxToCommonPart++;
		}
		String[] pathSection = (String[])ArrayUtils.subarray(pathParts, idxToCommonPart, pathParts.length);
		String endSubpart = StringUtils.join(pathSection, File.separator);
		String startSubpart = "";
		for(int i = 0; i < relativeToParts.length - idxToCommonPart; i++) {
			startSubpart += "../";
		}
		return startSubpart + endSubpart;
	}
	
	public static String removeRoot(File file, File root) {
		if(file == null) {
			throw new IllegalArgumentException("parameter file can not be null");
		}
		
		String absPath = file.getAbsolutePath();
		return absPath.replaceFirst(root.getAbsolutePath() + File.separator, "");
	}
}

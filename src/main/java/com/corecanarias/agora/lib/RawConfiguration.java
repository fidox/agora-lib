/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.util.LinkedHashMap;
import java.util.Map;

public class RawConfiguration extends BaseConfiguration {
	private Map<String, ModuleConfiguration> config;

	public RawConfiguration(Map<String, ModuleConfiguration> config) {
		this.config = config;
	}
	
	public RawConfiguration() {
		config = new LinkedHashMap<String, ModuleConfiguration>();
	}
	
	public void addProperty(String moduleName, String name, Object value) {
		BaseModuleConfiguration module = (BaseModuleConfiguration)config.get(moduleName);
		if(module == null) {
			module = new BaseModuleConfiguration(moduleName);
			config.put(moduleName, module);
		}
		module.put(name, value);
	}
	
	@Override
	public Map<String, ModuleConfiguration> getConfiguration() {
		return config;
	}
}
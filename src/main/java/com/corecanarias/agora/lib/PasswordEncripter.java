package com.corecanarias.agora.lib;

import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang.StringUtils;

public class PasswordEncripter {

	public static boolean test(String expected, String input) {
		if(isMD5(expected)) {
			try {
				input = MD5Crypt.crypt(input, extractSalt(expected));
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
		}
			
		return StringUtils.equals(expected, input);
	}

	private static String extractSalt(String expected) {
		String s = expected.substring(3);
		return s.substring(0, s.indexOf("$"));
	}

	private static boolean isMD5(String expected) {
		return expected != null && expected.startsWith("$1$");
	}
}
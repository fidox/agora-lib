/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Date utils
 * 
 * @author Israel E. Bethencourt (ieb@corecanarias.com)
 *
 */
public class CalendarUtils {

	public static final int DAY_END = 0;

	private CalendarUtils() {
		
	}
	
	/**
	 * Get the current year.
	 * @return Current year
	 */
	public static final int getYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}
	
	/**
	 * Add <code>days</code> days to current date 
	 * @param days Days to add to now
	 * @return The date 
	 */
	public static final Date addDays(int days) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

	/**
	 * Add <code>days</code> days to specified date 
	 * @param days Days to add to now
	 * @return The date 
	 */
	public static final Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

	/**
	 * Add the specified hour to a given date.
	 * 
	 * @param date The date to add the hour information
	 * @param dayPart DAY_ constants to define the hour to use
	 * @return The day with the hour defined
	 */
	public static final Date getDayAndHour(Date date, int dayPart) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		switch(dayPart) {
			case DAY_END:
				cal.set(Calendar.HOUR, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
			break;
		}
		return cal.getTime();
	}
	
	/**
	 * Get current date
	 * @return now
	 */
	public static final Date now() {
		return new Date();
	}

	/**
	 * Get the current month
	 * @return current month
	 */
	public static int getMonth() {
		return Calendar.getInstance().get(Calendar.MONTH) +1;
	}

	/**
	 * Get the current day.
	 * 
	 * @return current dat
	 */
	public static int getDay() {
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}

	public static final String STANDARD_DATEFORMAT = "yyyyMMdd'T'HHmmss.SSS Z";
	
	public static String serializeDate(Date date) {
		
		return new SimpleDateFormat(STANDARD_DATEFORMAT).format(date);
	}
	
	public static final Date unserializeDate(String date) {
		
		return formatDate(STANDARD_DATEFORMAT, date);
	}
	
	public static Date formatDate(String format, String date) {
		try {
			return new SimpleDateFormat(format).parse(date);
		} catch (ParseException e) {
			throw new RuntimeException("Unable to parse date: " + date + " valid format is " + format, e);
		}		
	}
	
	public static final boolean dateIsValid(String format, String date) {
		if(date == null) {
			return false;
		}
		try {
			new SimpleDateFormat(format).parse(date);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
}
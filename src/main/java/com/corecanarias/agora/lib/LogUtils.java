/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Israel E. Bethencourt (ieb@corecanarias.com)
 *
 */
public class LogUtils {
	
	private LogUtils() {
		
	}
	
	/**
	 * Instantiate a new logger
	 * @param clazz The class to lo
	 * @return The logger
	 */
	public static final Logger getLog(Class clazz) {
		return Logger.getLogger(clazz.getName());
	}
	
	/**
	 * Log an exception
	 * 
	 * @param log Logger
	 * @param msg Msg to log
	 * @param e Exception to log
	 */
	public static final void error(Logger log, String msg, Throwable e) {
		log.log(Level.SEVERE, msg, e);
	}
	
	public static final String getMark() {
		return String.valueOf(System.currentTimeMillis());
	}
}
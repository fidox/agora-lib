package com.corecanarias.agora.lib;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.corecanarias.agora.client.ProgressBarView;

public abstract class Executor<T> {
	private ThreadPoolExecutor executor;
	private int timeout;
	private int maxAttempts = 1;
	private int attemptsInterval = 1000;
	private Log log = null;
	private T result;
	private ExecutionException executionExcepton = null;
	private boolean isTimedOut = false;
	private ProgressBarView progressBar = null;
	
	public static final int FOREVER = -1;
	
	private Runnable operation = new Runnable() {

		public void run() {
			executionExcepton = null;
			isTimedOut = false;
			int timeElapsed = timeout;
			java.util.concurrent.Future<T> f = executor.submit(new Callable<T>() {

				public T call() {
					
					return exec(); 
				}			
			});
			if(timeout == FOREVER) {
				progressActivate();
				timeElapsed = 1;
				while(!f.isDone()) {
					defaultSleep();					
				}				
			} else {
				progressSetMax(timeElapsed / 100);
				while(!f.isDone() && timeElapsed > 0) {
					defaultSleep();
					progressStep();
					timeElapsed -= 100; 
				}				
			}
			
			if(timeout == FOREVER) {
				progressStop();				
			} else {
				progressClear();				
			}
			if(timeElapsed > 0) {
				try {
					result = f.get();
				} catch (InterruptedException e) {
					return;
				} catch (ExecutionException e) {
					executionExcepton = e;
					onError(e);
					return;
				}
				done(result);
			} else {
				isTimedOut = true;
				f.cancel(true);
				onTimeout();
			}
		}			
	};
	private Thread executorThread;
	private String name;

	public Executor(String name) {
		timeout = 5000;		
		executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		executor.prestartCoreThread();
		this.name = name;
	}
	
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	public void setMaxAttempts(int maxAttempts) {
		this.maxAttempts = maxAttempts;
	}
	
	public abstract T exec();
	
	public void done(T o) {
	}

	public void onError(ExecutionException e) {
		getLog().error("onError: " + name + " " + e.getMessage());
	}

	public void onTimeout() {
		getLog().error("Timeout: " + name);
	}

	public void executeAndWait() throws ExecutionException {
		executeMaxAttempts(operation);
		if(executionExcepton != null) {
			throw executionExcepton;
		}
	}

	private void executeMaxAttempts(Runnable op) throws ExecutionException {
		
		int attempts = 0;
		while(true) {
			if(attempts < maxAttempts || maxAttempts == -1) {
				op.run();
				if(executionExcepton != null || isTimedOut()) {
					attempts++;
					sleep(attemptsInterval);
				} else {
					return;
				}
			} else {
				return;
			}
		}
	}
	
	public void execute() {
		executorThread = new Thread(operation);
		executorThread.start();
	}

	public boolean isTimedOut() {
		return isTimedOut;
	}
	
	public T get() {
	
		if(executorThread != null && executorThread.isAlive()) {
			try {
				executorThread.wait();
			} catch (InterruptedException e) {
				
			}
		}
		return result;			
	}
	
	public void setProgressBar(ProgressBarView progressBar) {
		this.progressBar = progressBar;
	}
	
	protected void progressSetMax(int max) {
		if(progressBar != null) {
			progressBar.setMax(max);				
		}
	}
	
	protected void progressClear() {
		if(progressBar != null) {
			progressBar.clear();
		}		
	}
	
	protected void progressStep() {		
		if(progressBar != null) {
			progressBar.step();
		}
	}
	
	protected void progressActivate() {
		if(progressBar != null) {
			progressBar.activateProgressBar();
		}		
	}

	protected void progressStop() {
		if(progressBar != null) {
			progressBar.stopProgressBar();
		}
	}

	private void defaultSleep() {
		sleep(100);
	}
	
	public static void sleep(long val) {
		try {
			Thread.sleep(val);
		} catch (InterruptedException e) {
		}		
	}
	
	private Log getLog() {
		if(log == null)
			log = LogFactory.getLog(getClass());
		return log;
	}	
}

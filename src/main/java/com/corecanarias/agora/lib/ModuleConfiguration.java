/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

public interface ModuleConfiguration {
	
	String getName();
	
	Object getProperty(String name);

	String[] getPropertyNames();
	
	int getPropertyCount();
	
	/**
	 * Get a single property as string specifying a default value
	 * @param name the property name
	 * @param defaultValue If the property is not defined. this value is returned instead.
	 * @return The value of the module/name or null if doesn't exists
	 * @return
	 */
	String getPropertyAsString(String name, String defaultValue);
	String getPropertyAsString(String name);

	/**
	 * Get a multi-value property. You can define multi-value properties as follow:
	 * 
	 " <property name="prop-list"> 
	 "    <value>val1</value> 
	 "    <value>val2</value> 
	 "    <value>val3</value>
     "  </property>
	 *  
	 * 
	 * @param name Property name defined as list
	 * @return The value list
	 */
	String[] getPropertyAsList(String name);
	
	/**
	 * Get a single property as boolean. If the parameter is not defined returns false
	 * 
	 * @param name The property name
	 * @return true if the property is 'true' false otherwise
	 */
	boolean getPropertyAsBoolean(String name);
	
	/**
	 * Get a single property as boolean. If the parameter is not defined returns the default value
	 * 
	 * @param defaultValue the default value
	 * @param name The property name
	 * @return the property value of module/name or defaultValue is the property is not defined
	 */
	boolean getPropertyAsBoolean(String name, boolean defaultValue);
	
	/**
	 * Get a single property as integer
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	int getPropertyAsInt(String name, int defautlValue);
	
	/**
	 * Get a single property as float
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	float getPropertyAsFloat(String name, float defautlValue);
	
	/**
	 * Get a single property as double
	 * @param name The property name
	 * @param defautlValue The value if property doesn't exists
	 * @return The property value as integer or defaultValue is the property doesn't exists
	 */
	double getPropertyAsDouble(String name, double defautlValue);
	
	String printConfigurationData();
}
package com.corecanarias.agora.lib;

import java.io.IOException;
import java.net.InetAddress;

public class NetUtil {

	public static boolean ping(String addr) {

		try {
			InetAddress in = InetAddress.getByName(addr);
			return in.isReachable(1000);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}

package com.corecanarias.agora.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

public class SSHClient {
	private Connection connection;
	private Session session;
	private BufferedReader stdoutReader;
	private BufferedReader stderrReader;

	/**
	 * Connects and authenticate against a server. 
	 * 
	 * @param hostname The ip of the server to connect
	 * @param username Username to authenticate
	 * @param password Password to autenticate 
	 * @throws IOException Authentication error 
	 */
	public void open(String hostname, String username, String password) throws IOException {
		
		connection = new Connection(hostname);
		connection.connect();

		if(!connection.authenticateWithPassword(username, password)) {
			throw new IOException("Authentication error: username:password does not match");
		}
		createSession();
	}

	private void createSession() throws IOException {
		session = connection.openSession();
		InputStream stdout = new StreamGobbler(session.getStdout());
		InputStream stderr = new StreamGobbler(session.getStderr());

		stdoutReader = new BufferedReader(new InputStreamReader(stdout));
		stderrReader = new BufferedReader(new InputStreamReader(stderr));
	}
	
	public int execCommand(String cmd) throws IOException {
		session.execCommand(cmd);
		int count = 0;
		while(session.getExitStatus() == null) {
			count++;
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
			if(count >= 100) {
				return 0;
			}
		}
		return session.getExitStatus();
	}
	
	public BufferedReader getStderrReader() {
		return stderrReader;
	}
	
	public String getStderr() throws IOException {
		return getReaderAsString(stderrReader);		
	}
	
	public void setStdoutReader(BufferedReader stdoutReader) {
		this.stdoutReader = stdoutReader;
	}
	
	public String getStdout() throws IOException {
		return getReaderAsString(stdoutReader);		
	}
	
	public String getReaderAsString(BufferedReader reader) throws IOException {
		StringBuilder res = new StringBuilder();
        while (true) {
	        String line = reader.readLine();
	        if (line == null) {
                break;
	        }
	        res.append(line).append("\n");
        }

		return res.toString();
	}
	
	public void close() {
		if(session != null) {
			session.close();			
		}
		
		if(connection != null) {
			connection.close();			
		}
	}
}
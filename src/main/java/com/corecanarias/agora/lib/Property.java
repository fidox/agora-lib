/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Property<T> {
	private String name;
	private T value;
	
	public Property() {
		
	}
	
	public Property(String name, T value) {
		setName(name);
		setValue(value);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	
	@SuppressWarnings("rawtypes")
	public static final List<Property> fromMap(Map<?, ?> map) {
		ArrayList res = new ArrayList();
		for(Entry<?, ?> p: map.entrySet()) {
			res.add(new Property((String)p.getKey(), p.getValue()));
		}
		return res;
	}
	
	public static final Map asMap(List<Property> properties) {
		HashMap res = new HashMap();
		for(Property p: properties) {
			res.put(p.getName(), p.getValue());
		}
		return res;
	}
	
	public static final Map asMap(Property[] properties) {
		HashMap res = new HashMap();
		for(Property p: properties) {
			res.put(p.getName(), p.getValue());
		}
		return res;
	}
	
	@Override
	public String toString() {
		return String.format("[name: %s, value=%s]", name, value);
	}
}
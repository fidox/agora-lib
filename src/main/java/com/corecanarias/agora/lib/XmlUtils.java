package com.corecanarias.agora.lib;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XmlUtils {

	public static String documentToString(Document xml, boolean pretty) {
		xml.normalizeDocument();
		TransformerFactory tranFactory = TransformerFactory.newInstance(); 
		Transformer aTransformer = null;
		try {
			String xslt = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output method=\"xml\" omit-xml-declaration=\"yes\"/><xsl:strip-space elements=\"*\"/><xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template></xsl:stylesheet>";
			aTransformer = tranFactory.newTransformer(new StreamSource(new StringReader(xslt)));
		} catch (TransformerConfigurationException e) {
			throw new RuntimeException(e);
		}
		if(pretty) {
			aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		}
		Source src = new DOMSource(xml); 
		StringWriter outText = new StringWriter();
		Result dest = new StreamResult(outText); 
		try {
			aTransformer.transform(src, dest);
		} catch (TransformerException e) {
			throw new RuntimeException(e);
		} 
		return outText.toString();
	}
	
	public static Document stringToDocument(String xml) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder parser = factory.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
			Document origXml = parser.parse(is);
			
			return origXml;
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
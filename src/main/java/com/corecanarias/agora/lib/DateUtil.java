package com.corecanarias.agora.lib;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static Date parse(String s) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return formatter.parse(s);
	}
	
	public static String toString(Date d) {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return formatter.format(d);
	}
	
	public static final String formatHMS(double secs) {
		long totalSecs = Math.round(secs);
		long h = 0;
		long m = 0;
		long s = 0;
		
		m = totalSecs / 60;
		s = totalSecs % 60;
		if(m > 59) {
			h = m / 60;
			m -= (h * 60);
		}
		return String.format("%02d:%02d:%02d", h, m, s);
	}
}
/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

public class BaseModuleConfiguration implements ModuleConfiguration {
	private String name;
	private Map<String, Object> config = new HashMap<String, Object>();

	public BaseModuleConfiguration(String name, Map<String, Object> config) {
		this(name);
		if(config != null) {
			this.config = config;			 
		}
	}
	
	public BaseModuleConfiguration(String name) {
		this.name = name;
		config = new HashMap<String, Object>();
	}
	
	public void put(String name, Object value) {
		config.put(name, value);
	}
	
	public String getName() {
		return name;
	}

	public String[] getPropertyNames() {
		if(config == null) {
			return new String[] {};
		}
		return config.keySet().toArray(new String[] {});
	}

	public int getPropertyCount() {
		if(config == null) {
			return 0;
		}
		return config.size();
	}
	
	public Object getProperty(String name) {
		return config.get(name);
	}

	public String getPropertyAsString(String name, String defaultValue) {
		String value = getPropertyAsString(name);

		if(value == null) {
			return defaultValue;
		}
		return value;
	}

	public String getPropertyAsString(String name) {
		Object value = getProperty(name);
		return (String)value;
	}

	public String[] getPropertyAsList(String name) {
		Object value = config.get(name);
		if(value == null) {
			return null;
		}
		List<String> list = (List<String>)value;
		return list.toArray(new String[] {});
	}

	public boolean getPropertyAsBoolean(String name) {
		return getPropertyAsBoolean(name, false);
	}

	public boolean getPropertyAsBoolean(String name, boolean defaultValue) {
		if(getProperty(name) instanceof Boolean) {
			return (Boolean)getProperty(name);
		}
		String value = getPropertyAsString(name);
		if(value == null) {
			return false;
		}
		return "true".equals(value);
	}

	public int getPropertyAsInt(String name, int defaultValue) {
		if(getProperty(name) instanceof Integer) {
			return (Integer)getProperty(name);
		}
		String value = getPropertyAsString(name);
		if(value == null) {
			return defaultValue;
		}
		try {
			return Integer.valueOf(value);			
		} catch(NumberFormatException e) {
			throw new ConfigurationException("Unable to convert " + name + " to integer: " + value, e);
		}
	}

	public float getPropertyAsFloat(String name, float defaultValue) {
		if(getProperty(name) instanceof Float) {
			return (Float)getProperty(name);
		}
		String value = getPropertyAsString(name);
		if(value == null) {
			return defaultValue;
		}
		try {
			return Float.valueOf(value);			
		} catch(NumberFormatException e) {
			throw new ConfigurationException("Unable to convert " + name + " to float: " + value, e);
		}
	}

	public double getPropertyAsDouble(String name, double defaultValue) {
		if(getProperty(name) instanceof Double) {
			return (Double)getProperty(name);
		}
		String value = getPropertyAsString(name);
		if(value == null) {
			return defaultValue;
		}
		try {
			return Double.valueOf(value);			
		} catch(NumberFormatException e) {
			throw new ConfigurationException("Unable to convert " + name + " to double: " + value, e);
		}
	}

	public String printConfigurationData() {
		if(config == null) {
			return "";
		}
		
		StringBuffer res = new StringBuffer();
		res.append(String.format("[%s] Module configuration: ", getName()));
		for(Entry<String, Object> e: config.entrySet()) {
			res.append(e.getKey()).append(" = ").append(e.getValue()).append("\n");
		}
		return res.toString();
	}

	public Map<String, Object> getConfig() {
		return config;
	}
	
	@Override
	public String toString() {
		String res = getName() + ":[";
		for(Entry<String, Object> p: config.entrySet()) {
			res += p.getKey() + "=\"" + p.getValue() + "\",";
		}
		return res + "]";
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			return true;
		}
		ModuleConfiguration module = (ModuleConfiguration) obj;
		if(!StringUtils.equals(name, module.getName())) {
			return false;
		}
		if(module.getPropertyCount() != getPropertyCount()) {
			return false;
		}
		for(String prop: module.getPropertyNames()) {
			if(!getProperty(prop).equals(module.getProperty(prop))) {
				return false;
			}
		}
		return true;
	}
}
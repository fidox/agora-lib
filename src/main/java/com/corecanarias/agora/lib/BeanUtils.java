package com.corecanarias.agora.lib;

import java.lang.reflect.Field;

public class BeanUtils {

	public static void setField(String fieldName, Object instance, Object value) {
		try {
			Field field = instance.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(instance, value);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}

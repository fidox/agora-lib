package com.corecanarias.agora.lib;

import java.io.File;

public interface ZipUtilOutputFilter {

	File outputFilter(String file);

}

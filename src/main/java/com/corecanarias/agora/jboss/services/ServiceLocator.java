/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.jboss.services;

import java.io.File;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Access to local InitialContext
 * 
 * @author Israel E. Bethencourt (ieb@corecanarias.com)
 *
 */
public class ServiceLocator {
	private static ServiceLocator self = null;
	private InitialContext initialContext;
	private ServiceLocatorSettings settings;
	
	private ServiceLocator() {
		try {
			File f = new File(ServiceLocatorSettings.FILENAME);
			if(f.exists()) {
				settings = new ServiceLocatorSettings();
				initialContext = new InitialContext(settings.getInitialContext());
			} else {
				initialContext = new InitialContext();				
			}

		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	};
	
	public ServiceLocatorSettings getSettings() {
		return settings;
	}
	
	public static ServiceLocator getInstance() {
		if(self == null) {
			self = new ServiceLocator();
		}
		return self;
	}
	
	public InitialContext getInitialContext() {
		return initialContext;
	}
	
	public Object lookup(String name) {
		try {
			return initialContext.lookup(name);
		} catch (NamingException e) {
			throw new RuntimeException("Unable to locate service " + name, e);
		}
	}
}
/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.jboss.services;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.corecanarias.agora.lib.ModuleConfiguration;
import com.corecanarias.agora.lib.XmlConfiguration;

/** 
 * Default implementation for ConfigurationServiceMBean
 * 
 * @author Israel E. Bethencourt (ieb@corecanarias.com)
 *
 */
public class ConfigurationService implements ConfigurationServiceMBean {
	private String configurationFile;
	private Element root;
	private XmlConfiguration config;

	public void start() {
		File config = new File(getConfigurationFile());
		if(!config.exists() || !config.canRead()) {
			throw new RuntimeException("Unable to read configuration file: " + config.getAbsolutePath());
		}
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(config);
			root = doc.getDocumentElement();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		root.normalize();		
		updateProperties();
	}

	public Map<String, ModuleConfiguration> updateProperties() {
		config = new XmlConfiguration();
		return config.updateProperties(root);
	}
	
	public void stop() {
		root = null;
	}
	
	public ModuleConfiguration getModule(String module) {
		if(config == null) {
			return null;
		}
		return config.getModule(module);
	}

	public String printConfigurationData() {
		return config.printConfigurationData();
	}

	public String getConfigurationFile() {
		return configurationFile;
	}

	public void setConfigurationFile(String filename) {
		this.configurationFile = filename;
	}
	
	public Map<String, ModuleConfiguration> getConfiguration() {
		return config.getConfiguration();
	}
}

/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.jboss.services;

import java.util.Map;

import com.corecanarias.agora.lib.ModuleConfiguration;

/**
 * The configuration service, where any module can get/store specific configuration.
 * The configuraton file is configuration-service.xml located in the root of the ear.
 * 
 * The configuration is into the <configuration> tags. Inside it you can create a new 
 * section for your module:
 * <module name="your_module_name"></module>
 *  
 * Inside it you can add all properties you want. Like that:
 * <module ...>
 *    <property name="name_prop" value="value_prop" />
 * 
 * All properties are stored as strings, but you have helper methods to convert to any other
 * format: getAsxxx()
 * @author ieb@corecanarias.com
 *
 */
public interface ConfigurationServiceMBean {
	
	/**
	 * Getter for the configuration file property
	 * @return Path to configuration file
	 */
	String getConfigurationFile();
	
	/**
	 * Set the configuration file path.
	 * @param path and filename to configuration file
	 */
	void setConfigurationFile(String filename);
	
	/**
	 * Parse the xmo configuration and store the values
	 * into the configuration HashMap
	 *  
	 * @return The HashMap with the configuration
	 */
	Map<String, ModuleConfiguration> updateProperties();

	Map<String, ModuleConfiguration> getConfiguration();

	/**
	 * Get all configuration for a specific module
	 * @param module The module name
	 * @return The HashMap with all properties for that module
	 */
	ModuleConfiguration getModule(String module);

	/**
	 * Activate the service
	 */
	void start();
	
	/**
	 * Stop service
	 */
	void stop();
	
	/**
	 * Show the configuration in the jmx console
	 * @return
	 */
	String printConfigurationData();
}
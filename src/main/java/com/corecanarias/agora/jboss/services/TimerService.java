/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.jboss.services;

import java.util.Date;

import com.corecanarias.agora.jboss.services.EjbTimer;
import com.corecanarias.agora.jboss.services.ServiceLocator;

/**
 * TimerService configured by JBoss service. You must define the EjbTimer jndi to be
 * invoked as a param
 * 
 * @author Israel E. Bethencourt (ieb@corecanarias.com)
 *
 */
public class TimerService implements org.jboss.varia.scheduler.Schedulable {
	private String ejb;

	public TimerService(String ejb) {
		this.ejb = ejb;
	}

	public void perform(Date arg0, long arg1) {
		EjbTimer invoking = (EjbTimer)ServiceLocator.getInstance().lookup(ejb);
		invoking.invoke();
	}
}
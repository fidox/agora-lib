package com.corecanarias.agora.jboss.services;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ServiceLocatorSettings {
	public static final String FILENAME = "servicelocator_renderflow.properties";
	private boolean setDefault = false;
	private PropertiesConfiguration config;

	public ServiceLocatorSettings() {
		initializeFile();
		
		try {
			config = new PropertiesConfiguration(FILENAME);
		} catch (ConfigurationException e) {
			throw new RuntimeException(e);
		}

		if(setDefault) {
			setDefaultConfiguration();			
		}
	}
	
	private void setDefaultConfiguration() {
		
		setDefault = false;
		config.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		config.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
		config.setProperty("java.naming.provider.url", "api.renderflow.com:1099");
		try {
			config.save();
		} catch (ConfigurationException e) {
			throw new RuntimeException(e);
		}
	}

	private void initializeFile() {
		File f = new File(FILENAME);
		if(!f.exists()) {
			try {
				f.createNewFile();
				setDefault = true;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public PropertiesConfiguration getConfig() {
		return config;
	}
	
	public Hashtable<String,String> getInitialContext() {
		Hashtable<String,String> result = new Hashtable<String, String>();
		Iterator<String> iter = config.getKeys("java");
		while(iter.hasNext()) {
			String key = iter.next();
			result.put(key, config.getString(key, ""));
		}
			
		return result;
	}
}

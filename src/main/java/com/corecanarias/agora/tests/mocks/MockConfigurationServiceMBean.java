/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.tests.mocks;

import java.util.LinkedHashMap;
import java.util.Map;

import com.corecanarias.agora.jboss.services.ConfigurationServiceMBean;
import com.corecanarias.agora.lib.BaseModuleConfiguration;
import com.corecanarias.agora.lib.ModuleConfiguration;

public class MockConfigurationServiceMBean implements ConfigurationServiceMBean {
	private Map<String, ModuleConfiguration> props = new LinkedHashMap<String, ModuleConfiguration>();
	
	public void addProperty(String moduleName, String name, Object value) {
		BaseModuleConfiguration module = (BaseModuleConfiguration)props.get(moduleName);
		if(module == null) {
			module = new BaseModuleConfiguration(moduleName);
			props.put(moduleName, module);
		}
		module.put(name, value);
	}
	
	public void setProps(Map<String, ModuleConfiguration> props) {
		this.props = props;
	}
	
	public String getConfigurationFile() {
		return null;
	}

	public void setConfigurationFile(String filename) {
		
	}

	
	public Map<String, ModuleConfiguration> updateProperties() {
		return null;
	}

	public ModuleConfiguration getModule(String module) {
		if(props == null) {
			return null;
		}
		return props.get(module);
	}

	public void start() {
		
	}

	public void stop() {
		
	}

	public String printConfigurationData() {
		return null;
	}

	public Map<String, ModuleConfiguration> getConfiguration() {
		return props;
	}
}
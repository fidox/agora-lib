package com.corecanarias.agora.tests.mocks;

import java.util.Date;

public class BeanInnerSample {
	private String stringValue = "string value inner";
	private int intValue = 15;
	private Date dateValue = new Date();
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public int getIntValue() {
		return intValue;
	}
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}
	public Date getDateValue() {
		return dateValue;
	}
	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}
	
	
}

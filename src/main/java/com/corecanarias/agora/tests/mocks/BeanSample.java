package com.corecanarias.agora.tests.mocks;

import java.util.Date;

public class BeanSample {
	private String stringValue = "string value";
	private int intValue = 5;
	private Date dateValue = new Date();
	private BeanInnerSample bean = new BeanInnerSample();
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public int getIntValue() {
		return intValue;
	}
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}
	public Date getDateValue() {
		return dateValue;
	}
	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}
	public BeanInnerSample getBean() {
		return bean;
	}
	public void setBean(BeanInnerSample bean) {
		this.bean = bean;
	}
	
	
}
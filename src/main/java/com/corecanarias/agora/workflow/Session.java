/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.workflow;

import java.util.HashMap;
import java.util.Map;

/**
 * Workflow session.
 * 
 * Store variable session. Persistent over all workflow
 * 
 * @author Israel E. Bethencourt (ieb@corecanarias.com)
 *
 */
public class Session {
	private Context context;
	private Map<String, Object> variables;
	
	public Session(Context context) {
		this.context = context;
		variables = new HashMap<String, Object>(); 
	}
	
	/**
	 * Get the context
	 * @return
	 */
	public Context getContext() {
		return context;
	}
	
	/**
	 * Store a variable giving a name.
	 * If the variable exists the old value is replaced.
	 * 
	 * @param name The variable name
	 * @param instance the instance of the variable
	 */
	public Session putVariable(String name, Object instance) {
		variables.put(name, instance);
		return this;
	}
	
	public Object get(String name) {
		return variables.get(name);
	}
	
	public String getAsString(String name) {
		Object value = variables.get(name);
		if(value == null) {
			return null;
		}
		return value.toString();
	}
}
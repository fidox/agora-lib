/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.workflow;

import com.corecanarias.agora.lib.ModuleConfiguration;

public abstract class SessionCommand implements Command {
	private Session session;
	private boolean required = true;
	private boolean terminal = false;
	private ModuleConfiguration configuration;
	
	public final void setSession(Session session) {
		this.session = session;
	}
	
	public final Session getSession() {
		return session;
	}
	
	public Context getContext() {
		return session.getContext();
	}
	
	public boolean isTerminal() {
		return terminal;
	}
	public void setTerminal(boolean terminal) {
		this.terminal = terminal;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public boolean isRequired() {
		return required;
	}
	
	public void setConfiguration(ModuleConfiguration config) {
		this.configuration = config;
	}

	public ModuleConfiguration getConfiguration() {
		return configuration;
	}
}
/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.workflow;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;

import com.corecanarias.agora.lib.Configuration;
import com.corecanarias.agora.lib.LogUtils;
import com.corecanarias.agora.lib.ModuleConfiguration;


public class Executor {
	private Session session;
	private Map<String, Object> global;
	
	public Executor() {
		
	}
	
	public Executor(Session session) {
		this.session = session;
	}
	
	public void setSession(Session session) {
		this.session = session;
	}
	
	private void applyGlobalSession(Session session) {
		if(global == null) {
			return;
		}
		for(Entry<String, Object> e: global.entrySet()) {
			session.putVariable(e.getKey(), e.getValue());			
		}
	}
	
	public Command execute(Command cmd) {
		return execute(cmd, null);
	}
	
	public Command execute(Command cmd, ModuleConfiguration config) {
		cmd.setConfiguration(config);
		applyGlobalSession(session);
		
		if(cmd instanceof SessionCommand) {
			((SessionCommand)cmd).setSession(session);
		}
		try {
			cmd.execute();			
		} catch(RuntimeException e) {
			if(cmd.isRequired()) {
				throw e;				
			} else {
				LogUtils.getLog(Executor.class).log(Level.WARNING, "Unable to execute command " + cmd.getClass(), e);
			}
		}
		return cmd;
	}

	public Command execute(Class<?> clazz) {
		return execute(clazz, null);
	}
	
	public Command execute(Class<?> clazz, ModuleConfiguration config) {
		
		try {
			Object cmd = clazz.newInstance();
			if(!(cmd instanceof Command)) {
				throw new WorkflowException("Object " + cmd.getClass() + " is not an command");
			}
			return execute((Command)cmd, config);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public Command execute(String className) {
		return execute(className, null);
	}

	public Command execute(String className, ModuleConfiguration config) {
		try {
			Class<?> clazz = Class.forName(className);
			return execute(clazz, config);
		} catch (ClassNotFoundException e) {
			throw new WorkflowException("Class can not be found " + className);
		}
	}
	
	public void setGlobal(ModuleConfiguration moduleConfiguration) {
		if(moduleConfiguration == null) {
			return;
		}
		this.global = new HashMap<String, Object>();
		String[] names = moduleConfiguration.getPropertyNames();
		for(String name: names) {
			global.put(name, moduleConfiguration.getProperty(name));
		}
	}
	
	public Command[] execute(Configuration configuration) {
		String[] modules = configuration.getModules();
		Command[] cmds = new Command[modules.length];
		int i = 0;
		setGlobal(configuration.getModule("global"));
		for(String m: modules) {			
			if(!StringUtils.equals(m, "global")) {
				if(StringUtils.isBlank(configuration.getPropertyAsString(m, "command"))) {
					throw new WorkflowException("Command " + m + " has not defined command parameter");
				}
				Command cmd = execute(configuration.getPropertyAsString(m, "command", null), configuration.getModule(m));
				cmds[i++] = cmd;
				if(cmd.isTerminal()) {
					break;
				}
			}
		}
		return cmds;
	}
}
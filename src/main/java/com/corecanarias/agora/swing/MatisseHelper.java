package com.corecanarias.agora.swing;

import java.awt.Component;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MatisseHelper extends JPanel {
	private static final long serialVersionUID = -8663924389399304051L;
	private Log log = null;
	private JPanel panel;
	private HashMap<String,Object> cache = null;

	public MatisseHelper(JPanel panel) {
	
		this(panel, true);
	}

	public MatisseHelper(JPanel panel, boolean layout) {
		
		if(layout) {
			setLayout(new GridLayout(1, 1));			
		}
		this.panel = panel;		
		add(panel);
		cache = new HashMap<String,Object>();
		populateCache(panel);
	}

	public Object getWidgetById(String id) {
		
		Object obj = cache.get(id);
		if(obj == null) {
			throw new RuntimeException("Objeto no encontrado: " + id);
		} else {
			return obj;
		}
	}

	public List<Component> getAllComponents() {
		
		List<Component> result = new ArrayList<Component>();
		
		for(Object o: cache.values()) {
			
			if(o instanceof Component && !StringUtils.isEmpty(((Component)o).getName())) {
				result.add((Component)o);
			}
		}
		
		return result;
	}

	public void clearCache() {

		cache = new HashMap<String,Object>();

		populateCache(panel);
	}
	
	private void populateCache(JComponent panel) {

		Object obj = null;
		for(int i = 0; i < panel.getComponentCount(); i++) {
			if(!StringUtils.isEmpty((panel.getComponent(i).getName()))) {
				cache.put(panel.getComponent(i).getName(), panel.getComponent(i));
			}
			
			if(SwingUtils.isContainer(panel.getComponent(i)) && obj == null) {
				populateCache((JComponent)panel.getComponent(i));
			} 
		}
	}	

	protected Log getLog() {
		if (log == null) {
			log = LogFactory.getLog(getClass());
		}
		return log ;
	}
}

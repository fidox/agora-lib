package com.corecanarias.agora.swing;

import java.util.List;
import java.util.Vector;

import javax.swing.AbstractListModel;

public class MessageListModel extends AbstractListModel {
	private static final long serialVersionUID = -5931838029566440113L;
	private List<String> messages;
	
	public MessageListModel() {
		messages = new Vector<String>();
	}
	
	@Override
	public int getSize() {
		return messages.size();
	}

	@Override
	public Object getElementAt(int index) {
		return messages.get(index);
	}

	public void addMessage(String msg) {
		messages.add(msg);
		fireIntervalAdded(this, messages.size() -1, messages.size());
	}
}
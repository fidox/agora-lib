package com.corecanarias.agora.swing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.table.AbstractTableModel;

/**
 * Modelo para jtable para mostrar elementos clave=valor
 * 
 * @author ieb@corecanarias.com
 *
 */
public class PropertyTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 2225009192686645468L;

	private String[] columnNames = new String[] {"Nombre", "Valor"};

	private Map<String, ?> data;
	private List<String> names;
	private List<Object> values;
	
	public PropertyTableModel(Map<String, ?> data) {
		this.data = data;
		if(data == null) {
			this.data = new HashMap<String, Object>();
		}
		names = new ArrayList<String>();
		values = new ArrayList<Object>();
		for(Entry<String, ?> prop: this.data.entrySet()) {
			names.add(prop.getKey());
			values.add(prop.getValue());
		}			
	}
	
	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return names.get(rowIndex);
			case 1:
				return values.get(rowIndex);
			default:
				return "";
		}
	}
	
	public void setColumnNames(String nameColumn, String valueColumn) {
		this.columnNames = new String[] {nameColumn, valueColumn};
	}
}
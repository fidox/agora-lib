/*
 * Created on 11/12/2006 17:30:20
 *
 * 
 *                                    Israel E. Bethencourt
 *                                    CORE be digital, S.L.
 *                                                 (c) 2005
 */
package com.corecanarias.agora.swing;


public interface AgoraForm {

	boolean onClose();	
	boolean onOpen();

}

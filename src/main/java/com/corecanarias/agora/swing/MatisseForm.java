/*
 * Created on 11/12/2006 17:27:35
 *
 * 
 *                                    Israel E. Bethencourt
 *                                    CORE be digital, S.L.
 *                                                 (c) 2005
 */
package com.corecanarias.agora.swing;

import java.awt.Component;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationContext;

import com.corecanarias.agora.client.subsystem.ui.WidgetLocator;

public class MatisseForm extends JPanel implements AgoraForm, WidgetLocator {
	private static final long serialVersionUID = -1997529802521288638L;
	private JPanel panel;
	private MatisseHelper matisse;
	private Log log;
	private ApplicationContext context;

	public MatisseForm(JPanel panel) {
		matisse = new MatisseHelper(panel);

		configure();
		
		setLayout(new GridLayout(1, 1));
		this.panel = panel;

		add(panel);
		context = Application.getInstance().getContext();
	}

	protected void configure() {
		
	}

	public JPanel getPanel() {
		return panel;
	}

	public Object getWindow() {

		return ApplicationMainFrame.getInstance().getFrame();
	}

	public boolean onClose() {

		return true;
	}

	public boolean onOpen() {

		return true;
	}

	protected void init() {

	}

	protected void clearCache() {
		matisse.clearCache();
	}

	public Object getWidgetById(String id) {
		return matisse.getWidgetById(id);
	}

	public List<Component> getAllComponents() {

		return matisse.getAllComponents();
	}

	public void addActionMap(String actionName, String component, Class clazz) {

		Action action = getApplicationAction(actionName, clazz);
		JComponent c = (JComponent)getWidgetById(component);

		if (c instanceof AbstractButton) {
			AbstractButton b = (AbstractButton) c;
			if (action.getValue(Action.NAME) == null) {
				action.putValue(Action.NAME, b.getText());
			}

			if (action.getValue(Action.LARGE_ICON_KEY) == null) {
				action.putValue(Action.LARGE_ICON_KEY, b.getIcon());
			}

			b.setAction(action);
		}

		if (action.getValue(Action.ACCELERATOR_KEY) != null) {
			setInputMap(action.getValue(Action.ACCELERATOR_KEY).toString(), actionName, component, clazz);
		}
	}

	public Action getApplicationAction(String name, Class clazz) {
		return context.getActionMap(clazz, this).get(name);
	}

	public void setInputMap(String keystroke, String action, String component, Class clazz) {

		JComponent comp = (JComponent) getWidgetById(component);

		comp.getActionMap().put(action, getApplicationAction(action, clazz));
		comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(keystroke), action);
	}

	
	protected Log getLog() {
		if (log == null) {
			log = LogFactory.getLog(getClass());
		}
		return log;
	}
}

package com.corecanarias.agora.swing;

import org.jdesktop.application.ResourceConverter;
import org.jdesktop.application.ResourceMap;

public class JComponentResourceConverter extends ResourceConverter {

	public JComponentResourceConverter() {
		super(java.awt.Component.class);
	}

	@Override
	public Object parseString(String arg0, ResourceMap arg1) throws ResourceConverterException {
		return null;
	}
}

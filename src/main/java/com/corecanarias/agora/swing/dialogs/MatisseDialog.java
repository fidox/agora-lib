package com.corecanarias.agora.swing.dialogs;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.WindowEvent;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceMap;

import com.corecanarias.agora.client.subsystem.ui.WidgetLocator;
import com.corecanarias.agora.swing.ApplicationMainFrame;
import com.corecanarias.agora.swing.MatisseHelper;

public abstract class MatisseDialog extends JDialog implements WidgetLocator {
	private static final long serialVersionUID = -8733203129945304617L;
	private JPanel panel;
	private MatisseHelper matisse;
	private boolean result = false;
	private ApplicationContext context;
	private ResourceMap resources;

	private transient Log logger;

	/**
	 * Este constructor usa como padre la ventana prinicpal
	 * 
	 * @param title
	 * @param modal
	 */
	public MatisseDialog(String title, boolean modal) {

		this(ApplicationMainFrame.getInstance().getFrame(), title, modal);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}

	/**
	 * Con este constructor puedes especificar el padre del dialogo
	 * 
	 * @param parent
	 * @param title
	 * @param modal
	 */
	public MatisseDialog(Frame parent, String title, boolean modal) {
		super(parent, title, modal);
	}

	public MatisseDialog(Dialog parent, String title, boolean modal) {
		super(parent, title, modal);
	}

	public void initForm(JPanel content) {
		matisse = new MatisseHelper(content);

		this.panel = content;

		setContentPane(panel);
		pack();
		setLocationRelativeTo(ApplicationMainFrame.getInstance().getFrame());

		this.context = org.jdesktop.application.Application.getInstance().getContext();

		this.resources = context.getResourceMap(this.getClass());
		resources.injectComponents(this);
		resources.injectFields(this);
		init();
	}

	/**
	 * 
	 *
	 */
	public abstract void init();

	/**
	 * 
	 *
	 */
	public boolean onSetVisible() {
		return true;
	}

	/**
	 * 
	 *
	 */
	public boolean open() {
		if (onSetVisible()) {
			setVisible(true);
		}
		return result;
	}

	/**
	 * 
	 * @param res
	 */
	public void accept() {

		result = true;
		close();
	}

	public void cancel() {
		result = false;
		close();
	}

	private void close() {
		// setVisible(false);
		// // Chapuza de Sun: setVisible(false);
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
		dispose();
	}

	protected ApplicationContext getContext() {
		return context;
	}

	public Action getApplicationAction(String name) {

		return getApplicationAction(name, getClass());
	}

	public Action getApplicationAction(String name, Class clazz) {

		return context.getActionMap(clazz, this).get(name);
	}

	public void setInputMap(String keystroke, String action, String component) {

		JComponent comp = (JComponent) getWidgetById(component);

		comp.getActionMap().put(action, getApplicationAction(action));
		comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(keystroke), action);
	}

	public void addActionMap(String actionName, String component) {

		addActionMap(actionName, component, getClass());
	}

	public void addActionMap(String actionName, String component, Class clazz) {

		Action action = getApplicationAction(actionName, clazz);
		if (action == null) {
			throw new RuntimeException("Action " + actionName
					+ " does not exists");
		}
		JComponent c = (JComponent) getWidgetById(component);

		if (c instanceof AbstractButton) {
			AbstractButton b = (AbstractButton) c;
			if (action.getValue(Action.NAME) == null) {
				action.putValue(Action.NAME, b.getText());
			}

			if (action.getValue(Action.LARGE_ICON_KEY) == null) {
				action.putValue(Action.LARGE_ICON_KEY, b.getIcon());
			}

			b.setAction(action);
		}

		if (action.getValue(Action.ACCELERATOR_KEY) != null) {
			setInputMap(action.getValue(Action.ACCELERATOR_KEY).toString(),
					actionName, component);
		}
	}

	public Object getWidgetById(String id) {
		return matisse.getWidgetById(id);
	}

	public MatisseHelper getMatisse() {
		return matisse;
	}

	public Object getWindow() {

		return this;
	}

	protected Log getLog() {
		if (logger == null) {
			logger = LogFactory.getLog(getClass());
		}
		return logger;
	}
}

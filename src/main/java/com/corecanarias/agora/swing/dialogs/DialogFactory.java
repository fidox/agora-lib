package com.corecanarias.agora.swing.dialogs;

import javax.swing.JFileChooser;

public class DialogFactory {

	public static final JFileChooser createDirChooser() {
		JFileChooser chooser = new JFileChooser();
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    chooser.setAcceptAllFileFilterUsed(false);

	    return chooser;
	}
	
	public static final JFileChooser createFileChooser() {
		JFileChooser chooser = new JFileChooser();
	    chooser.setCurrentDirectory(new java.io.File("."));

	    return chooser;		
	}
}

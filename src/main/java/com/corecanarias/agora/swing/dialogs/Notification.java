package com.corecanarias.agora.swing.dialogs;

import javax.swing.JOptionPane;

import com.corecanarias.agora.swing.ApplicationMainFrame;

public class Notification {

	public static boolean ask(String msg) {
		return JOptionPane.showConfirmDialog(ApplicationMainFrame.getInstance().getFrame(), msg, "Confirmación", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}
	
	public static void msg(String msg) {
		JOptionPane.showMessageDialog(ApplicationMainFrame.getInstance().getFrame(), msg);
	}
}
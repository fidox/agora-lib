/*
 * Created on 14-abr-2006 21:16:17
 *
 * 
 *                                    Israel E. Bethencourt
 *                                    CORE be digital, S.L.
 *                                                 (c) 2005
 */
package com.corecanarias.agora.swing.dialogs;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;

import org.jdesktop.application.Action;

import com.corecanarias.agora.swing.SwingUtils;
import com.corecanarias.agora.swing.dialogs.matisse.ErrorPanel;

public class ErrorDialog extends MatisseDialog {
	private static final long serialVersionUID = 6369293734507786870L;
	private Throwable ex;
	private String message;
	
	public ErrorDialog(String msg, Throwable e) {
		this("System error", msg, e);
	}

	public ErrorDialog(String title, String msg, Throwable e) {
		this(title, msg, e, true);
	}

	public ErrorDialog(String title, String msg, Throwable e, boolean modal) {
		super(title, modal);
		this.message = msg;			
		this.ex = e;
		initForm(new ErrorPanel());		
	}

	@Override
	public void init() {
		addActionMap("onAccept", "AcceptButton");
				
		((JButton)getWidgetById("ExitButton")).setVisible(false);
		getTitleLabel().setText("   " + getTitle());
		getMessageText().setEditable(false);
		getMessageText().setContentType("text/html"); 
		getMessageText().setText(message);
		getMessageText().addHyperlinkListener(new HyperlinkListener() {

			@Override
			public void hyperlinkUpdate(HyperlinkEvent arg0) {

				if(arg0.getEventType() == EventType.ACTIVATED) {
					openUrl(arg0.getURL());
				}
			}
		});
		getTextArea().setText("No info available");

		if (ex != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintWriter pw = new PrintWriter(baos);
			ex.printStackTrace(pw);
			pw.flush();
			
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			BufferedReader br = new BufferedReader(new InputStreamReader(bais));
			
			try {
				String s = br.readLine();
				String info = "";
				info = info + "\n" + s;
				while(s != null) {
					
					s = br.readLine();
					info = info + "\n" + s;
				}
				getTextArea().setText(info);
			} catch (IOException e) {
				return;
			}
		}
		getTextArea().setCaretPosition(0);
	}
	
	@Action
	public void onAccept() {
		
		accept();
	}
	
	protected JTextArea getTextArea() {
		return (JTextArea)getWidgetById("TextArea");
	}
	
	protected JEditorPane getMessageText() {
		return (JEditorPane)getWidgetById("LabelMessage");
	}

	protected JLabel getTitleLabel() {
		return (JLabel)getWidgetById("LabelErrorTitle");
	}
	
	private void openUrl(URL url) {
		SwingUtils.openURL(url.toString());
	}			
}

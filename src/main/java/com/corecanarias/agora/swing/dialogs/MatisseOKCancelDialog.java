package com.corecanarias.agora.swing.dialogs;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

import org.jdesktop.application.Action;

public abstract class MatisseOKCancelDialog extends MatisseDialog {
	private static final long serialVersionUID = -7447132066481726640L;

	public MatisseOKCancelDialog(String title, boolean modal) {
		super(title, modal);
	}

	public MatisseOKCancelDialog(JDialog parent, String title, boolean modal) {
		super(parent, title, modal);
	}
	
	public MatisseOKCancelDialog(JFrame parent, String title, boolean modal) {
		super(parent, title, modal);
	}
	
	@Override
	public void init() {
				
		addActionMap("acceptAction", "AcceptButton", MatisseOKCancelDialog.class);
		addActionMap("cancelAction", "ExitButton", MatisseOKCancelDialog.class);
		
		addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent e) {
			}

			public void windowClosed(WindowEvent e) {
			}

			public void windowClosing(WindowEvent e) {
				onClose();
			}

			public void windowDeactivated(WindowEvent e) {
			}

			public void windowDeiconified(WindowEvent e) {
			}

			public void windowIconified(WindowEvent e) {
			}

			public void windowOpened(WindowEvent e) {
			}			
		});
	}

	public JButton getButtonOK() {
		return (JButton)getWidgetById("AcceptButton");
	}
	
	public JButton getButtonCancel() {
		return (JButton)getWidgetById("ExitButton");
	}
	
	@Action
	public void acceptAction() {

		accept();
	}
	
	@Action
	public void cancelAction() {
		
		cancel();
	}
	
	@Override
	public void accept() {

		if(onAccept() == false) {
			return;			
		}
		super.accept();
	}
	
	@Override
	public void cancel() {
	
		if(onCancel() == false) {
			return;			
		}
		super.cancel();
	}

	public abstract boolean onClose();

	public abstract boolean onAccept();
	
	public abstract boolean onCancel();
}

/*
 * Created on 11/12/2006 19:40:55
 *
 * 
 *                                    Israel E. Bethencourt
 *                                    CORE be digital, S.L.
 *                                                 (c) 2005
 */
package com.corecanarias.agora.swing;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JViewport;

public class SwingUtils {

	public static void openURL(String url) {
		java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
		try {
			desktop.browse(new URI(url));
		} catch (IOException e1) {
			throw new RuntimeException(e1);
		} catch (URISyntaxException e1) {
			throw new RuntimeException(e1);
		}
	}

	public static final boolean isContainer(Object o) {
		
		if(o == null)
			return false;
		return (o instanceof JPanel) || (o instanceof JScrollPane) || (o instanceof JViewport) || (o instanceof JTabbedPane) || (o instanceof JToolBar) || (o instanceof JSplitPane);
	}
}

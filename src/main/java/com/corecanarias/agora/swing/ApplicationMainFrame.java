package com.corecanarias.agora.swing;

import javax.swing.JFrame;

public class ApplicationMainFrame {
	private JFrame frame;
	private static ApplicationMainFrame self = null;
	
	private ApplicationMainFrame() {
		
	}
	
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public static ApplicationMainFrame getInstance() {
		if(self == null) {
			self = new ApplicationMainFrame();			
		}
		return self;
	}
}

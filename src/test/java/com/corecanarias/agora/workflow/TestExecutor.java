package com.corecanarias.agora.workflow;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.corecanarias.agora.lib.BaseModuleConfiguration;
import com.corecanarias.agora.lib.Configuration;
import com.corecanarias.agora.lib.MBeanConfiguration;
import com.corecanarias.agora.lib.RawConfiguration;
import com.corecanarias.agora.tests.mocks.MockConfigurationServiceMBean;
import com.corecanarias.agora.tests.utils.SampleCommand;
import com.corecanarias.agora.tests.utils.SampleErrorCommand;
import com.corecanarias.agora.tests.utils.SampleSessionCommand;


public class TestExecutor {
	private Session session;
	private Executor exec;

	@Before
	public void setup() {
		session = new Session(null);
		
		exec = new Executor();
		exec.setSession(session);
	}
	
	@Test
	public void testExecuteCommand() {
		SampleCommand cmd = new SampleCommand();
		exec.execute(cmd);
		assertTrue(cmd.isExecuted());
	}

	@Test
	public void testExecuteSessionCommand() {
		exec.execute(new SampleSessionCommand());
		boolean executed = (Boolean)session.get("executed");
		assertTrue(executed == true);
	}
	
	@Test
	public void testExecuteAsClass() {
		exec.execute(SampleSessionCommand.class);
		boolean executed = (Boolean)session.get("executed");
		assertTrue(executed == true);		
	}
	
	@Test(expected=WorkflowException.class)
	public void testExecuteAsClassNoCommandInstance() {
		exec.execute(String.class);
	}

	@Test
	public void testExecuteAsString() {
		
		exec.execute("com.corecanarias.agora.tests.utils.SampleSessionCommand");
		boolean executed = (Boolean)session.get("executed");
		assertTrue(executed == true);		
	}
	
	@Test(expected=WorkflowException.class)
	public void testExecuteWithStringNoClass() {
		exec.execute("no-class");		
	}
	
	@Test(expected=RuntimeException.class)
	public void testExecuteErrorRequired() {

		exec.execute(SampleErrorCommand.class);
	}
	
	@Test
	public void testExecuteErrorNotRequired() {

		SampleErrorCommand cmd = new SampleErrorCommand();
		cmd.setRequired(false);
		exec.execute(cmd);
	}

	@Test
	public void testExecuteConfiguration() {
		session.putVariable("executed", false);
		MockConfigurationServiceMBean configService = new MockConfigurationServiceMBean();
		configService.addProperty("SampleSessionCommand1", "command", "com.corecanarias.agora.tests.utils.SampleSessionCommand");

		Configuration commandConfig = new MBeanConfiguration(configService);
		exec.execute(commandConfig);
		
		boolean executed = (Boolean)session.get("executed");
		assertTrue(executed == true);
		
	}

	@Test
	public void testExecuteConfigurationOrdered() {
		session.putVariable("executed", false);
		session.putVariable("order", "");

		RawConfiguration commandConfig = new RawConfiguration();
		commandConfig.addProperty("SampleSessionCommand1", "command", "com.corecanarias.agora.tests.utils.SampleSessionCommand");
		commandConfig.addProperty("SampleSessionCommand1", "order", "1");
		commandConfig.addProperty("SampleSessionCommand2", "command", "com.corecanarias.agora.tests.utils.SampleSessionCommand");
		commandConfig.addProperty("SampleSessionCommand2", "order", "2");
		exec.execute(commandConfig);
		
		assertEquals("12", (String)session.get("order"));		
	}
	
	@Test(expected=RuntimeException.class)
	public void testExecuteError() {
		session.putVariable("executed", false);
		
		RawConfiguration commandConfig = new RawConfiguration();
		commandConfig.addProperty("Error1", "command", "com.corecanarias.agora.tests.utils.SampleErrorCommand");
		
		exec.execute(commandConfig);
	}
	
	@Test(expected=WorkflowException.class)
	public void testExecuteNoCommandDefined() {
		session.putVariable("executed", false);

		RawConfiguration commandConfig = new RawConfiguration();
		commandConfig.addProperty("Error1", "", "");
		exec.execute(commandConfig);
	}
	
	@Test
	public void testExecuteGlobalConfiguration() {
		BaseModuleConfiguration global = new BaseModuleConfiguration("global");
		global.put("global1", "value1");
		
		exec.setGlobal(global);
		
		SampleCommand cmd = new SampleCommand();
		exec.execute(cmd);
		
		assertEquals("value1", (String)session.get("global1"));		
	}
	
	@Test
	public void testExecuteGlobalWithConfiguration() {
		session.putVariable("executed", false);
		session.putVariable("order", "");

		RawConfiguration commandConfig = new RawConfiguration();
		commandConfig.addProperty("global", "global1", "value1");
		commandConfig.addProperty("SampleSessionCommand1", "command", "com.corecanarias.agora.tests.utils.SampleSessionCommand");
		exec.execute(commandConfig);
		
		assertEquals("value1", (String)session.get("global1"));		
	}
}
package com.corecanarias.agora.workflow;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSession {

	@Test
	public void test_put_string_variable() {
		Session session = new Session(null);
		session.putVariable("string-var", "value");
		
		assertEquals("value", session.getAsString("string-var"));
	}

	@Test
	public void test_get_unexistent_string_var() {
		Session session = new Session(null);
		
		assertNull(session.getAsString("string-var"));		
	}
}
package com.corecanarias.agora;

import java.io.IOException;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

import org.junit.Test;

public class TestGroovy {

	@Test
	public void testGroovy() throws IOException, ResourceException, ScriptException {
		String[] roots = new String[] { "src/test/groovy" };
		GroovyScriptEngine gse = new GroovyScriptEngine(roots);
		Binding binding = new Binding();
		Object res = gse.run("main.groovy", binding);
		
	}
}
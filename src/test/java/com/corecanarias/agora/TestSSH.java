package com.corecanarias.agora;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.Test;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

import com.corecanarias.agora.lib.SSHClient;

public class TestSSH {

	public void test() throws IOException {
		Connection conn = new Connection("192.168.20.79");

		/* Now connect */

		conn.connect();
		boolean isAuthenticated = conn.authenticateWithPassword("render", "render");
		if (isAuthenticated == false) {
			throw new IOException("Authentication failed.");
		}

		Session sess = conn.openSession();
		sess.execCommand("cmd /c test");

		/*
		 * This basic example does not handle stderr, which is sometimes
		 * dangerous (please read the FAQ).
		 */

		InputStream stdout = new StreamGobbler(sess.getStdout());
		InputStream stderr = new StreamGobbler(sess.getStderr());

		BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(stdout));
		BufferedReader stderrReader = new BufferedReader(new InputStreamReader(stderr));

		while (true) {
			String line = stdoutReader.readLine();
			if (line == null)
				break;
			System.out.println(line);
		}
        while (true) {
	        String line = stderrReader.readLine();
	        if (line == null)
	                break;
	        System.out.println(line);
        }

		/* Show exit status, if available (otherwise "null") */

		System.out.println("ExitCode: " + sess.getExitStatus());

		/* Close this session */

		sess.close();

		/* Close the connection */

		conn.close();
	}
	
	public void test2() throws IOException {
		SSHClient sshClient = new SSHClient();
		sshClient.open("192.168.20.79", "render", "render");
		int exitCode = sshClient.execCommand("/home/render/bin/rdf-shutdown.sh");
		System.out.println("Exit code: " + exitCode + " " + sshClient.getStderr());
		System.out.println(sshClient.getStdout());
		sshClient.close();
	}
	
	@Test
	public void dummy() {
		
	}
}
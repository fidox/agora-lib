package com.corecanarias.agora.lib;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.corecanarias.agora.tests.mocks.BeanSample;

public class TestTemplateUtils {

	@Test
	public void test() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("var1", "replace1");
		assertEquals("replace1.ok", TemplateUtils.replaceVars("${var1}.ok", params));
	}
	
	@Test
	public void testCopyProperties() {
		BeanSample sample = new BeanSample();
		Map<String, String> res = TemplateUtils.getProperties("sample", sample);
		System.out.println(res);
		
	}
}

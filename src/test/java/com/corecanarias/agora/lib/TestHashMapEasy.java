package com.corecanarias.agora.lib;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestHashMapEasy {

	@Test
	public void testGetDefaultValue() {
		HashMapEasy<String,String> map = new HashMapEasy<String,String>();

		String res = map.get("not-exists", "defaultValue");
		assertEquals("defaultValue", res);
	}
	
	@Test
	public void testGetAsInteger() {
		HashMapEasy<String,String> map = new HashMapEasy<String,String>();
		map.put("int", "1");
		
		Integer res = map.getAsInteger("int");
		assertEquals(1, res, 0);
	}
	
	@Test
	public void testAsIntegerDefault() {
		HashMapEasy<String,String> map = new HashMapEasy<String,String>();

		Integer res = map.getAsInteger("int", 2);
		assertEquals(2, res, 0);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetAsIntegerANotInteger() {
		HashMapEasy<String,String> map = new HashMapEasy<String,String>();
		map.put("int", "a");
		
		map.getAsInteger("int");
	}
	
	@Test
	public void testGetAsIntegerDefaultANotInteger() {
		HashMapEasy<String,String> map = new HashMapEasy<String,String>();
		map.put("int", "a");
		
		Integer res = map.getAsInteger("int", 3);
		assertEquals(3, res, 0);				
	}
}
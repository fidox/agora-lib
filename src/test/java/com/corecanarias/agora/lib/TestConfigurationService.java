/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.corecanarias.agora.jboss.services.ConfigurationService;

public class TestConfigurationService {
	private ConfigurationService subject;

	@Before
	public void setUp() throws Exception {
		subject = new ConfigurationService();
		subject.setConfigurationFile("src/test/resources/config.xml");
		subject.start();
	}

	@Test
	public void testReadConfig() {
		BaseModuleConfiguration expectedMod1 = new BaseModuleConfiguration("module1");
		expectedMod1.put("int", "10");
		expectedMod1.put("string", "string value");
		expectedMod1.put("boolean", "true");
		expectedMod1.put("double", "4.3");

		ModuleConfiguration module1 = subject.getModule("module1");
	
		assertEquals(expectedMod1, module1);
	}
	
	@Test
	public void testReadList() {
		BaseModuleConfiguration expectedMod1 = new BaseModuleConfiguration("module2");
		expectedMod1.put("string", "string value 2");
		List<String> expcList = new ArrayList<String>();
		expcList.add("val1");
		expcList.add("val2");
		expcList.add("val3");
		expectedMod1.put("list", expcList);
		ModuleConfiguration module1 = subject.getModule("module2");
		assertEquals(expectedMod1, module1);		
	}
}
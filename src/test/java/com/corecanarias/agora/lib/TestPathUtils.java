package com.corecanarias.agora.lib;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class TestPathUtils {

	@Test
	public void testsplitExtension() {
		String f = "ext.jpg";
		assertEquals(".jpg", PathUtils.splitExtension(f)[1]);
	}

	@Test
	public void testWindowsStandardPath() {

		assertEquals("/prueba/prueba.txt", PathUtils.standardPath("c:\\prueba\\prueba.txt"));
	}
	
	@Test
	public void testEmptyStandardPath() {
		assertEquals("", PathUtils.standardPath(""));		
	}
	
	@Test
	public void testWindowsServerPath() {
		assertEquals("/prueba/prueba.txt", PathUtils.standardPath("\\\\server\\prueba\\prueba.txt"));				
	}
	
	@Test
	public void testLinuxPath() {
		assertEquals("/path/prueba/prueba.txt", PathUtils.standardPath("/path/prueba/prueba.txt"));						
	}
	
	@Test
	public void testBuildPath() {
		String res = PathUtils.buildPath("a", "b");
		assertEquals(String.format("a%sb", File.separator), res);
	}

	@Test
	public void testBuildPathSingleParam() {
		String res = PathUtils.buildPath("a");
		assertEquals(String.format("a", File.separator), res);
	}

	@Test
	public void testBuildPathNull() {
		String res = PathUtils.buildPath(null);
		assertEquals("", res);
	}

	@Test
	public void testPathRelativeTo() {
		
		String res = PathUtils.pathRelativeTo(new File("/home/test/file/relative/INPUT/rdf.max"), new File("/home/test/file/relative/INPUT"));
		assertEquals("rdf.max", res);
	}
	
	@Test
	public void testPathRelativeToMaps() {
		String res = PathUtils.pathRelativeTo(new File("/username/MAPS/filename.jpg"), new File("/username/SERVER/1/2/INPUT"));
		assertEquals("../../../../MAPS/filename.jpg", res);
	}

	@Test
	public void testRelativeToRoot() throws IOException {
		String res = PathUtils.pathRelativeTo(new File("/username/MAPS/filename.jpg"), new File("/other/SERVER/1/2/INPUT"));
		assertEquals("../../../../../username/MAPS/filename.jpg", res);
	}

	@Test
	public void testRelativeToNull() {
		String res = PathUtils.pathRelativeTo(new File("/username/MAPS/filename.jpg"), null);
		assertEquals("filename.jpg", res);		
	}
	
	@Test
	public void testPartStartingWithSeparator() {
		String res = PathUtils.buildPath("a", "/b");
		assertEquals(String.format("a%sb", File.separator), res);		 
	}
	
	@Test
	public void testRemoveRoot() {
		
		String res = PathUtils.removeRoot(new File("/username/SERVER/ProjectId/workId/OUTPUT/layer/frame/file_out.1"), new File("/username/SERVER/ProjectId/workId/OUTPUT"));
		assertEquals("layer/frame/file_out.1", res);		
	}
}
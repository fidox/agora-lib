package com.corecanarias.agora.lib;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class TestXmlConfiguration {

	@Test
	public void testReadXml() throws FileNotFoundException {
		
		FileInputStream xml = new FileInputStream("src/test/resources/config.xml");
		XmlConfiguration config = new XmlConfiguration(xml);
		
		BaseModuleConfiguration expectedMod1 = new BaseModuleConfiguration("module1");
		expectedMod1.put("int", "10");
		expectedMod1.put("string", "string value");
		expectedMod1.put("boolean", "true");
		expectedMod1.put("double", "4.3");

		ModuleConfiguration module1 = config.getModule("module1");
		assertEquals(expectedMod1, module1);
	}
	
	@Test
	public void testReadXmlList() throws FileNotFoundException {
		
		FileInputStream xml = new FileInputStream("src/test/resources/config.xml");
		XmlConfiguration config = new XmlConfiguration(xml);
		
		BaseModuleConfiguration expectedMod2 = new BaseModuleConfiguration("module2");
		expectedMod2.put("string", "string value 2");
		List<String> listParams = new ArrayList<String>();
		listParams.add("val1");
		listParams.add("val2");
		listParams.add("val3");
		expectedMod2.put("list", listParams);
		ModuleConfiguration module2 = config.getModule("module2");
		assertEquals(expectedMod2, module2);
	}

	@Test
	public void testReadXmlSimpleList() throws FileNotFoundException {
		
		FileInputStream xml = new FileInputStream("src/test/resources/config.xml");
		XmlConfiguration config = new XmlConfiguration(xml);
		
		BaseModuleConfiguration expectedMod4 = new BaseModuleConfiguration("module4");
		List<String> listParams = new ArrayList<String>();
		listParams.add("val1");
		expectedMod4.put("simple-list", listParams);
		ModuleConfiguration module4 = config.getModule("module4");
		assertEquals(expectedMod4, module4);
	}

	@Test
	public void testReadXmlLongText() throws FileNotFoundException {
		
		FileInputStream xml = new FileInputStream("src/test/resources/config.xml");
		XmlConfiguration config = new XmlConfiguration(xml);
		
		BaseModuleConfiguration expectedMod3 = new BaseModuleConfiguration("module3");
		String s = "\n\t\t\tprueba\n\t\t";
		expectedMod3.put("long-text", s);
		ModuleConfiguration module3 = config.getModule("module3");
		assertEquals(expectedMod3, module3);
	}
	
	@Test
	public void testReadXmlCDATA() throws FileNotFoundException {
		
		FileInputStream xml = new FileInputStream("src/test/resources/config.xml");
		XmlConfiguration config = new XmlConfiguration(xml);
		
		BaseModuleConfiguration expectedMod5 = new BaseModuleConfiguration("module5");
		String s = "\n\t\t\t\n\t\t\t\t\tprintln \"Hello, World!\"\n\t\t\t\n\t\t";
		expectedMod5.put("cdata-value", s);
		ModuleConfiguration module5 = config.getModule("module5");
		assertEquals(expectedMod5, module5);
	}
}
/**
 * Copyright 2012 Israel E. Bethencourt (ieb@corecanarias.com)
 * 
 * This file is part of agora-lib.
 * 
 * agora-lib is free software: you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * agora-lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with agora-lib. 
 * If not, see http://www.gnu.org/licenses/.
 */ 
package com.corecanarias.agora.lib;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.corecanarias.agora.tests.mocks.MockConfigurationServiceMBean;

public class TestConfiguration {
	private Configuration subject;
	private MockConfigurationServiceMBean configService;

	@Before
	public void setUp() throws Exception {
		configService = new MockConfigurationServiceMBean();
		Map<String, ModuleConfiguration> properties = new HashMap<String, ModuleConfiguration>();
		
		BaseModuleConfiguration res = new BaseModuleConfiguration("module1");
		res.put("prop11", "val11");
		res.put("prop12", "val12");
		res.put("prop-boolean", "true");
		res.put("prop-int", "11");
		res.put("prop-float", "11.22");
		properties.put("module1", res);
		
		res = new BaseModuleConfiguration("module2");
		res.put("prop21", "val21");
		res.put("prop22", "val22");
		properties.put("module2", res);
		
		res = new BaseModuleConfiguration("module3");
		List<String> list = new ArrayList<String>();
		list.add("val1");
		list.add("val2");
		list.add("val3");
		res.put("proplist", list);
		properties.put("module3", res);
		
		configService.setProps(properties);
		subject = new MBeanConfiguration(configService);
	}

	/**
	 * Get the value of a module/property
	 */
	@Test
	public void testGetProperty() {

		assertEquals("val11", subject.getProperty("module1", "prop11"));
	}
	
	@Test
	public void testGetPropertyAsString() {
		assertEquals("val11", subject.getPropertyAsString("module1", "prop11", "defaultValue"));		
	}

	@Test
	public void testGetPropertyAsStringDefaultValue() {
		assertEquals("defaultValue", subject.getPropertyAsString("module1", "not-exists", "defaultValue"));		
	}

	@Test
	public void testGetPropertyAsList() {
		String[] expected = new String[] {"val1", "val2", "val3"}; 
		String[] res = subject.getPropertyAsList("module3", "proplist");
		
		Assert.assertArrayEquals(expected, res);
	}
	
	/**
	 * Should returns null when a unknown module is requested
	 */
	@Test
	public void testGetUnknownModule() {

		assertNull(subject.getProperty("module-unknown", "prop11"));
	}

	@Test
	public void testDefaultValueWhenModuleNotExists() {
		assertEquals("defaultValue", subject.getPropertyAsString("module-not-exists", "not-exists", "defaultValue"));				
	}
	
	/**
	 * Should return null when an unknow property is requested
	 */
	@Test
	public void testGetUnknownProperty() {

		assertNull(subject.getProperty("module1", "prop-unknown"));
	}
	
	@Test
	public void testGetAsBoolean() {

		assertEquals(true, subject.getPropertyAsBoolean("module1", "prop-boolean"));
	}
	
	@Test
	public void testGetAsInt() {
		assertEquals(11, subject.getPropertyAsInt("module1", "prop-int", 0));		
	}
	
	@Test
	public void testGetAsIntDefault() {
		assertEquals(12, subject.getPropertyAsInt("module1", "prop-unknown", 12));				
	}

	@Test
	public void testGetAsFloatDefault() {
		assertEquals(12.33F, subject.getPropertyAsFloat("module1", "prop-unknown", 12.33F), 0);				
	}

	@Test(expected = ConfigurationException.class)
	public void testGetAsIntFormatException() {
		assertEquals(11, subject.getPropertyAsInt("module1", "prop11", 0));		
	}

	@Test
	public void testGetAsFloat() {
		assertEquals(11.22F, subject.getPropertyAsFloat("module1", "prop-float", 0), 0);				
	}
}
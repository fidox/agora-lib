package com.corecanarias.agora.lib;


import java.security.NoSuchAlgorithmException;

import org.junit.Test;

public class TestMD5Crypt {

	@Test
	public void testDistinctSalt() throws NoSuchAlgorithmException {
		String salt2 = MD5Crypt.crypt("password");
		System.out.println(salt2);

		if(salt2.startsWith("$1$")) {
			String s = salt2.substring(3);
			s = s.substring(0, s.indexOf("$"));
			System.out.println("s: " + s);
			String salt4 = MD5Crypt.crypt("prueba", s);
			System.out.println(salt4);
		}
	}
}
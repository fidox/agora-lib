package com.corecanarias.agora.lib;

import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;

import org.junit.Test;

public class TestPasswordEncripter {

	@Test
	public void testClearPasswords() {
		
		boolean res = PasswordEncripter.test("clear-password", "clear-password");
		assertTrue(res);
	}
	
	@Test
	public void testMD5Passwords() throws NoSuchAlgorithmException {
		String clearPassword = "md5-password";
		String expected = MD5Crypt.crypt(clearPassword);

		boolean res = PasswordEncripter.test(expected, clearPassword);
		assertTrue(res);
	}
}

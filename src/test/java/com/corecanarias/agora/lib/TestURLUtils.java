package com.corecanarias.agora.lib;

import org.junit.Assert;
import org.junit.Test;

public class TestURLUtils {

	@Test
	public void testGetURLBase() {

		Assert.assertEquals("base.ext", URLUtils.getURLBase("http://parent.com/dir1/dir2/base.ext"));
		
	}
}
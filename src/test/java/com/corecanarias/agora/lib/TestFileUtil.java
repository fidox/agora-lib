package com.corecanarias.agora.lib;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestFileUtil {
	public static final String DIR = "src/test/resources/tmp";
	private File dir;
	
	@Before
	public void setUp() {
		dir = new File(DIR);
		FileUtil.deleteDir(dir);
		dir.mkdir();		
	}
	
	@After
	public void tearDown() {
		dir = new File(DIR);
		FileUtil.deleteDir(dir);		
	}
	
	@Test
	public void testFilenameNoExtensionExists() throws IOException {
		
		File f2 = new File(DIR, "f1.txt2");
		f2.createNewFile();
		
		boolean res = FileUtil.filenameNoExtensionExists(DIR, "f1.txt");
		assertTrue(res);
	}
	
	@Test
	public void testFilenameNoExtensionExistsFalse() throws IOException {
		
		File f2 = new File(DIR, "f2.txt");
		f2.createNewFile();
		
		boolean res = FileUtil.filenameNoExtensionExists(DIR, "f1.txt");
		assertFalse(res);
	}

	@Test
	public void testFilenameNoExtensionExistsAbsolutePath() throws IOException {
		
		File f2 = new File(DIR, "f1.txt2");
		f2.createNewFile();
		
		File f = new File(DIR, "f1.txt");
		boolean res = FileUtil.filenameNoExtensionExists(DIR, f.getAbsolutePath());
		assertTrue(res);
	}

	@Test
	public void testListDirectories() {
		
		File root = new File(DIR, "testSearch");
		root.mkdir();
		
		File dirToSearch = new File(root, PathUtils.buildPath("dir1", "dir2", "dir3"));
		dirToSearch.mkdirs();

		List<File> result = new ArrayList<File>();
		FileUtil.listDirectories(result, root, new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return "dir3".equals(pathname.getName());
			}			
		});
		
		for(File f: result) {
			try {
				System.out.println(f.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		assertEquals("The directory was not found " + dirToSearch, 1, result.size());
		assertEquals("The directory was not found " + dirToSearch, dirToSearch, result.get(0));
	}
}

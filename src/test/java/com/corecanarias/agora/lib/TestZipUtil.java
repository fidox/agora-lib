package com.corecanarias.agora.lib;


import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class TestZipUtil {

	public static final String[] FILES = new String[] {
		"src/test/resources/test-zip/folder1/folder1/file1.txt",
		"src/test/resources/test-zip/folder1/folder1/file2.txt",
		"src/test/resources/test-zip/folder1/file.txt",
		"src/test/resources/test-zip/folder2/file1.txt",
		"src/test/resources/test-zip/folder2/file2.txt",
		"src/test/resources/test-zip/folder2/file3.txt",
		"src/test/resources/test-zip/file1.txt",
		"src/test/resources/config.xml"
	};
	
	@Test
	public void testZipDirectory() throws IOException {
		createZipFile(FILES);
	}
	
	@Test
	public void testUnzip() throws IOException {
		final File dir = FileUtil.createTempDir("agora", "zip");
		dir.deleteOnExit();
		
		File f = createZipFile(FILES);
		ZipUtil.Unzip(f.getAbsolutePath(), null, new ZipUtilOutputFilter() {

			@Override
			public File outputFilter(String file) {
				return new File(PathUtils.buildPath(dir.getAbsolutePath(), file));
			}
		});
		FileUtil.deleteDir(dir);
	}
	
	private File createZipFile(String[] files) throws IOException {
		File f = File.createTempFile("agora", ".zip");
		f.deleteOnExit();
		
		ZipUtil.zip(files, f.getAbsolutePath(), new ZipUtilEntryName() {

			@Override
			public String getEntryName(File f) {
				int idx = f.getAbsolutePath().indexOf("test-zip/");
				if(idx != -1) {
					return f.getAbsolutePath().substring(idx);					
				} else {
					return f.getName();
				}
			}
		}, null);
		return f;		
	}
}
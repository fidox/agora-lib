package com.corecanarias.agora.tests.utils;

import com.corecanarias.agora.workflow.SessionCommand;

public class SampleSessionCommand extends SessionCommand {

	public void execute() {
	
		getSession().putVariable("executed", true);
		
		if(getConfiguration() != null) {
			String order = getSession().getAsString("order");
			order += getConfiguration().getProperty("order");
			getSession().putVariable("order", order);			
		}
	}
}
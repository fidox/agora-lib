package com.corecanarias.agora.tests.utils;

import com.corecanarias.agora.lib.ModuleConfiguration;
import com.corecanarias.agora.workflow.Command;

public class SampleCommand implements Command {
	private boolean executed = false;
	public void execute() {
		System.out.println("OK");
		executed = true;
	}
	
	public boolean isExecuted() {
		return executed;
	}

	public boolean isRequired() {
		return true;
	}

	public ModuleConfiguration getConfiguration() {
		return null;
	}

	public void setConfiguration(ModuleConfiguration config) {
		
	}

	@Override
	public boolean isTerminal() {
		return false;
	}
}
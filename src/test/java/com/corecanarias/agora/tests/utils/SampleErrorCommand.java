package com.corecanarias.agora.tests.utils;

import com.corecanarias.agora.workflow.SessionCommand;

public class SampleErrorCommand extends SessionCommand {

	public void execute() {

		System.out.println("Error");
		throw new RuntimeException("error");
	}
}
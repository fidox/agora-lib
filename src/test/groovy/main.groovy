/*

import java.util.ArrayList;
import java.util.Map;


def someBuilder = new NodeBuilder()

 render {
 global {
 }
 ConfigureDefaults(class='com.renderflow.server.impl.services.engine.commands.ConfigureDefaults') {
 }
 CreateCommandLine(class='com.renderflow.server.impl.services.engine.commands.RenderCommandBuilder') {
 "cmd-template": 'rdf-arnold "${user.username}" ${project.id} ${work.id} %%tp2'
 }
 GenerateTaskList(class='com.renderflow.server.impl.services.engine.commands.GenerateTaskList') {
 value: createRnderTaskList()
 }
 ComposeMessage(class='com.renderflow.server.impl.services.engine.commands.ComposeMessage') {
 }
 }
 */

import com.corecanarias.agora.lib.*

username="user"
projectId = 2
workId= 3
render = {
	RenderCommandBuilder("create the command line") {
		when {
			false
		}
		template "rdf-arnold ${username} ${projectId} ${workId} %%tp2"
	}
}

RawConfiguration config = new RawConfiguration()

def r = new NodeBuilder().invokeMethod(render)
walk(r)

public void walk(node) {
	print "${node.name()}\n"
	node.children().each {
		if(!it instanceof String) {
			walk(it)
		}
	}
}